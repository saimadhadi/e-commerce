package com.epam.ecommerce.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.epam.ecommerce.exception.CartException;
import com.epam.ecommerce.exception.InsufficientQuantityException;
import com.epam.ecommerce.exception.InvalidQuantityException;
import com.epam.ecommerce.exception.ProductException;
import com.epam.ecommerce.model.CartItemModel;
import com.epam.ecommerce.model.ProductModel;
import com.epam.ecommerce.model.UserCartModel;
import com.epam.ecommerce.service.CartService;
import com.epam.ecommerce.service.UserDetailsServiceImpl;
import com.epam.ecommerce.utilities.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CartControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Mock
	private CartService cartService;

	@Mock
	private UserDetailsServiceImpl userDetailsServiceImpl;

	@InjectMocks
	private CartController cartController;

	private static Long userId = 1L;
	private static Long cartItemId = 1L;
	private static Long productId = 1L;
	private static Long quantity = 1L;
	private static BigDecimal productPrice = new BigDecimal("1000.00");
	private static String productName = "Iphone X";
	private static Long productStock = 1L;
	private static BigDecimal cartTotalPrice = new BigDecimal("1000.00");

	private ObjectMapper mapper = new ObjectMapper();
	private static CartItemModel cartItemModel;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(cartController).build();
	}

	@BeforeAll
	public static void initTestdata() {
		cartItemModel = new CartItemModel(cartItemId, userId, productId, quantity,
				new ProductModel(productId, productName, productStock, productPrice));
	}

	@Test
	public void getUserCartTest() throws Exception {
		UserCartModel userCartModel = new UserCartModel(userId, new ArrayList<>(), cartTotalPrice);
		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.when(cartService.getUserCartModel(userId)).thenReturn(userCartModel);
		this.mockMvc.perform(get("/cart"))
				.andExpect(model().attribute(Constants.USER_CART_MODEL_ATTRIBUTE, userCartModel))
				.andExpect(view().name(Constants.CART_JSP)).andExpect(status().isOk());
	}

	@Test
	public void addOrEditCartItemOfUserTest() throws Exception {

		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.when(cartService.addItemToCart(cartItemModel.getUserId(), cartItemModel)).thenReturn(cartItemModel);

		this.mockMvc
				.perform(post("/cart").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel)))
				.andExpect(content().string(Constants.ITEM_ADDED + cartItemModel.getCartItemId()))
				.andExpect(status().isOk());
	}

	@Test
	public void addOrEditCartItemOfUserImproperQuantityExceptionTest() throws Exception {
		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.when(cartService.addItemToCart(cartItemModel.getUserId(), cartItemModel))
				.thenThrow(InvalidQuantityException.class);

		this.mockMvc
				.perform(post("/cart").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel)))
				.andExpect(content().string(Constants.ENTER_VALID_QUANTITY)).andExpect(status().isOk());
	}

	@Test
	public void addOrEditCartItemOfUserInsufficientQuantityExceptionTest() throws Exception {

		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.when(cartService.addItemToCart(cartItemModel.getUserId(), cartItemModel))
				.thenThrow(InsufficientQuantityException.class);

		this.mockMvc
				.perform(post("/cart").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel)))
				.andExpect(content().string(Constants.PRODUCT_OUT_OF_STOCK)).andExpect(status().isOk());
	}

	@Test
	public void addOrEditCartItemOfUserExceptionTest() throws Exception {
		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.when(cartService.addItemToCart(cartItemModel.getUserId(), cartItemModel))
				.thenThrow(ProductException.class);

		this.mockMvc
				.perform(post("/cart").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel)))
				.andExpect(content().string(Constants.ITEM_NOT_FOUND));
	}

	@Test
	public void editCartItemQuantityTest() throws Exception {
		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.when(cartService.editCartItemQuantity(cartItemModel.getUserId(), cartItemModel))
				.thenReturn(cartItemModel);
		this.mockMvc
				.perform(patch("/cart").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel)))
				.andExpect(content().string(Constants.QUANTITY_UPDATED));
	}

	@Test
	public void editCartItemQuantityImproperQuantityExceptionTest() throws Exception {
		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.when(cartService.editCartItemQuantity(cartItemModel.getUserId(), cartItemModel))
				.thenThrow(InvalidQuantityException.class);
		this.mockMvc
				.perform(patch("/cart").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel)))
				.andExpect(content().string(Constants.ENTER_VALID_QUANTITY));
	}

	@Test
	public void editCartItemQuantityInsufficientQuantityExceptionTest() throws Exception {
		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.when(cartService.editCartItemQuantity(cartItemModel.getUserId(), cartItemModel))
				.thenThrow(InsufficientQuantityException.class);
		this.mockMvc
				.perform(patch("/cart").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel)))
				.andExpect(content().string(Constants.PRODUCT_OUT_OF_STOCK));
	}

	@Test
	public void editCartItemQuantityExceptionTest() throws Exception {
		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.when(cartService.editCartItemQuantity(cartItemModel.getUserId(), cartItemModel))
				.thenThrow(CartException.class);
		this.mockMvc
				.perform(patch("/cart").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel)))
				.andExpect(content().string(Constants.ITEM_NOT_FOUND));
	}

	@Test
	public void deleteCartItemFromUserTest() throws JsonProcessingException, Exception {
		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.when(cartService.removeItemFromCart(cartItemModel.getUserId(), cartItemModel.getCartItemId()))
				.thenReturn(cartItemModel);
		this.mockMvc
				.perform(delete("/cart").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel.getCartItemId())))
				.andExpect(content().string(Constants.ITEM_REMOVED));

	}

	@Test
	public void deleteCartItemFromUserExceptionTest() throws JsonProcessingException, Exception {
		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.when(cartService.removeItemFromCart(cartItemModel.getUserId(), cartItemModel.getCartItemId()))
				.thenThrow(CartException.class);
		this.mockMvc
				.perform(delete("/cart").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel.getCartItemId())))
				.andExpect(content().string(Constants.ITEM_NOT_FOUND));

	}

}
