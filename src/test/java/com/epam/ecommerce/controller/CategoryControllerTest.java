package com.epam.ecommerce.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.epam.ecommerce.exception.CategoryException;
import com.epam.ecommerce.model.CategoryModel;
import com.epam.ecommerce.model.ProductModel;
import com.epam.ecommerce.service.CategoryService;
import com.epam.ecommerce.service.ProductService;
import com.epam.ecommerce.utilities.Constants;

class CategoryControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Mock
	private CategoryService categoryService;

	@Mock
	private ProductService productService;

	@InjectMocks
	private CategoryController categoryController;

	private Long productId = 1L;
	private String productName = "Iphone X";
	private Long productQuantity = 100L;
	private BigDecimal productPrice = new BigDecimal("100");

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(categoryController).build();
	}

	@Test
	public void getMainCategoryListTest() throws Exception {
		Long categoryId = 0L;
		CategoryModel categoryModelElectronics = new CategoryModel(1L, "Electronics");
		List<CategoryModel> categoryModelList = new ArrayList<>();
		categoryModelList.add(categoryModelElectronics);
		Mockito.when(categoryService.getCategoriesByParentCategoryId(categoryId)).thenReturn(categoryModelList);
		this.mockMvc.perform(get("/category").param("categoryId", Constants.ZERO_STRING)).andExpect(status().isOk())
				.andExpect(model().attribute("categoryList", categoryModelList))
				.andExpect(model().attribute(Constants.CATEGORY_TYPE, Constants.CATEGORIES))
				.andExpect(view().name(Constants.CATEGORIES_JSP));
	}

	@Test
	public void getSubCategoryListTest() throws Exception {
		Long categoryId = 1L;
		List<CategoryModel> categoryModelList = new ArrayList<>();
		CategoryModel categoryModelMobiles = new CategoryModel(2L, "Mobiles");
		categoryModelList.add(categoryModelMobiles);
		Mockito.when(categoryService.getCategoriesByParentCategoryId(categoryId)).thenReturn(categoryModelList);
		this.mockMvc.perform(get("/category").param("categoryId", Constants.ONE_STRING)).andExpect(status().isOk())
				.andExpect(model().attribute(Constants.CATEGORY_LIST_ATTRIBUTE, categoryModelList))
				.andExpect(model().attribute(Constants.CATEGORY_TYPE, Constants.SUB_CATEGORIES))
				.andExpect(view().name(Constants.CATEGORIES_JSP));
	}

	@Test
	public void getProductListTest() throws Exception {
		Long categoryId = 2L;
		List<ProductModel> productModelList = new ArrayList<>();
		productModelList.add(new ProductModel(productId, productName, productQuantity, productPrice));
		Mockito.when(productService.getProductsUnderCategory(categoryId)).thenReturn(productModelList);
		this.mockMvc.perform(get("/category").param("categoryId", Constants.TWO_STRING)).andExpect(status().isOk())
				.andExpect(model().attribute(Constants.PRODUCT_LIST_ATTRIBUTE, productModelList))
				.andExpect(view().name(Constants.PRODUCTS_JSP));
	}

	@Test
	public void getMainCategoryListTestException() throws Exception {
		Long categoryId = 1L;
		Mockito.when(categoryService.getCategoriesByParentCategoryId(categoryId)).thenThrow(CategoryException.class);
		this.mockMvc.perform(get("/category").param("categoryId", Constants.ONE_STRING)).andExpect(status().isOk())
				.andExpect(model().attribute(Constants.EXCEPTION_MESSAGE, Constants.ITEM_NOT_FOUND))
				.andExpect(view().name(Constants.ERROR_JSP));
	}

}