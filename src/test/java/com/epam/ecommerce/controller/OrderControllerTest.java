package com.epam.ecommerce.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.epam.ecommerce.exception.CartException;
import com.epam.ecommerce.exception.InsufficientQuantityException;
import com.epam.ecommerce.exception.OrderException;
import com.epam.ecommerce.model.OrderItemModel;
import com.epam.ecommerce.model.UserOrderModel;
import com.epam.ecommerce.service.OrderService;
import com.epam.ecommerce.service.UserDetailsServiceImpl;
import com.epam.ecommerce.utilities.Constants;
import com.fasterxml.jackson.databind.ObjectMapper;

class OrderControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Mock
	private OrderService orderService;

	@Mock
	private UserDetailsServiceImpl userDetailsServiceImpl;

	@InjectMocks
	private OrderController orderController;

	private ObjectMapper mapper = new ObjectMapper();

	private Long userId = 1L;
	private Long orderItemId = 1L;
	private String productName = "Iphone x";
	private BigDecimal orderItemPrice = new BigDecimal("1000.00");;
	private Long quantity = 1L;
	private Boolean isDelivered = Boolean.FALSE;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(orderController).build();
	}

	@Test
	public void getUserOrdersTest() throws Exception {
		UserOrderModel userOrderModel = new UserOrderModel(userId, new ArrayList<>());
		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.when(orderService.getUserOrderModel(userId)).thenReturn(userOrderModel);
		this.mockMvc.perform(get("/orders"))
				.andExpect(model().attribute(Constants.USER_ORDER_MODEL_ATTRIBUTE, userOrderModel))
				.andExpect(view().name(Constants.ORDERS_JSP));
	}

	@Test
	public void addOrderItemForUserTest() throws Exception {
		Long cartItemId = 1L;
		OrderItemModel orderItemModel = new OrderItemModel(orderItemId, userId, productName, orderItemPrice, quantity,
				isDelivered);
		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.when(orderService.addOrder(orderItemModel.getUserId(), cartItemId)).thenReturn(orderItemModel);
		this.mockMvc
				.perform(post("/orders").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemId)))
				.andExpect(content().string(Constants.ORDER_PLACED_MSG + orderItemId));

	}

	@Test
	public void addOrderItemForUserInsufficientQuantityExceptionTest() throws Exception {
		Long cartItemId = 1L;
		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.when(orderService.addOrder(userId, cartItemId)).thenThrow(InsufficientQuantityException.class);
		this.mockMvc
				.perform(post("/orders").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemId)))
				.andExpect(content().string(Constants.PRODUCT_OUT_OF_STOCK));
	}

	@Test
	public void addOrderItemForUserOrderCartExceptionTest() throws Exception {
		Long cartItemId = 1L;
		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.when(orderService.addOrder(userId, cartItemId)).thenThrow(CartException.class);
		this.mockMvc
				.perform(post("/orders").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemId)))
				.andExpect(content().string(Constants.ITEM_NOT_FOUND));
	}

	@Test
	public void addAllOrdersFromCartForUserTest() throws Exception {
		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.doNothing().when(orderService).addAllOrdersFromCartForUser(userId);
		this.mockMvc
				.perform(post("/orders/all").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(userId)))
				.andExpect(content().string(Constants.CHECKED_OUT_MSG));
	}

	@Test
	public void addAllOrdersFromCartForUserExceptionTest() throws Exception {
		Mockito.when(userDetailsServiceImpl.getLoggedInUserId(Mockito.any())).thenReturn(userId);
		Mockito.doThrow(OrderException.class).when(orderService).addAllOrdersFromCartForUser(userId);
		this.mockMvc.perform(
				post("/orders/all").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(userId)))
				.andExpect(content().string(Constants.CART_EMPTY));
	}

}