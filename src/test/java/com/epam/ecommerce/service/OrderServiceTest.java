package com.epam.ecommerce.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.ecommerce.entity.CartItem;
import com.epam.ecommerce.entity.OrderItem;
import com.epam.ecommerce.entity.Product;
import com.epam.ecommerce.exception.CartException;
import com.epam.ecommerce.exception.InsufficientQuantityException;
import com.epam.ecommerce.exception.OrderException;
import com.epam.ecommerce.exception.ProductException;
import com.epam.ecommerce.model.OrderItemModel;
import com.epam.ecommerce.model.UserOrderModel;
import com.epam.ecommerce.repository.CartRepository;
import com.epam.ecommerce.repository.OrderRepository;
import com.epam.ecommerce.repository.ProductRepository;

class OrderServiceTest {

	@Mock
	private OrderRepository orderRepository;

	@Mock
	private ProductService productService;

	@Mock
	private ProductRepository productRepository;

	@Mock
	private CartRepository cartRepository;

	@InjectMocks
	private OrderService orderService;

	private static Long userId = 1L;
	private static Long productId = 1L;
	private static Long quantity = 1L;
	private static Boolean isDelivered = false;
	private static String productName = "Iphone x";
	private static BigDecimal orderItemPrice = new BigDecimal("1000.00");
	private static BigDecimal productPrice = new BigDecimal("1000.00");
	private static Long totalUnits = 100L;
	private static Long orderItemId = 1L;
	private static Long cartItemId = 1L;

	private static OrderItem orderItem;
	private static OrderItemModel orderItemModel;
	private static Product product;
	private static CartItem cartItem;

	@BeforeAll
	public static void initializeData() {
		product = new Product(productName, productPrice, totalUnits);
		product.setProductId(productId);
		orderItem = new OrderItem(orderItemId, userId, productName, orderItemPrice, quantity, isDelivered);
		orderItemModel = new OrderItemModel(orderItemId, userId, productName, orderItemPrice, quantity, isDelivered);
		cartItem = new CartItem(cartItemId, userId, productId, quantity, product);
	}

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void addOrderTest() throws OrderException, CartException, InsufficientQuantityException {
		Mockito.when(cartRepository.findByCartItemIdAndUserId(cartItemId, userId))
				.thenReturn(Optional.ofNullable(cartItem));
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(product);
		Mockito.doNothing().when(cartRepository).delete(Mockito.any());
		Mockito.when(orderRepository.save(Mockito.any())).thenReturn(orderItem);
		assertEquals(orderItemModel, orderService.addOrder(userId, cartItemId));
	}

	@Test
	public void addOrderFailureTest() throws OrderException, CartException, InsufficientQuantityException {
		Long wrongUserId = 2L;
		orderItem = new OrderItem(orderItemId, wrongUserId, productName, orderItemPrice, quantity, isDelivered);
		Mockito.when(cartRepository.findByCartItemIdAndUserId(cartItemId, userId))
				.thenReturn(Optional.ofNullable(cartItem));
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(product);
		Mockito.doNothing().when(cartRepository).delete(Mockito.any());
		Mockito.when(orderRepository.save(Mockito.any())).thenReturn(orderItem);
		assertNotEquals(orderItemModel, orderService.addOrder(userId, cartItemId));
	}

	@Test
	public void addOrderInsufficientQuantityExceptionTest() {
		Long insifficientQuantity = 1000L;
		CartItem cartItem = new CartItem(cartItemId, userId, productId, insifficientQuantity, product);
		Mockito.when(cartRepository.findByCartItemIdAndUserId(cartItemId, userId))
				.thenReturn(Optional.ofNullable(cartItem));
		assertThrows(InsufficientQuantityException.class, () -> orderService.addOrder(userId, cartItemId));
	}

	@Test
	public void addOrderCartExceptionTest() {
		Mockito.when(cartRepository.findByCartItemIdAndUserId(cartItemId, userId))
				.thenReturn(Optional.ofNullable(null));
		assertThrows(CartException.class, () -> orderService.addOrder(userId, cartItemId));
	}

	@Test
	public void getUserOrderModelTest() throws ProductException, OrderException {
		List<OrderItemModel> orderItemModelList = new ArrayList<>();
		orderItemModelList.add(orderItemModel);
		List<OrderItem> orderItemList = new ArrayList<>();
		orderItemList.add(orderItem);
		UserOrderModel expected = new UserOrderModel(userId, orderItemModelList);
		Mockito.when(orderRepository.findByUserId(userId)).thenReturn(orderItemList);
		assertEquals(expected, orderService.getUserOrderModel(userId));
	}

	@Test
	public void getUserOrderModelFailureTest() throws OrderException {
		Long wrongQuantity = 2L;
		OrderItem orderItem = new OrderItem(orderItemId, userId, productName, orderItemPrice, wrongQuantity,
				isDelivered);
		List<OrderItemModel> orderItemModelList = new ArrayList<>();
		orderItemModelList.add(orderItemModel);
		List<OrderItem> orderItemList = new ArrayList<>();
		orderItemList.add(orderItem);
		UserOrderModel expected = new UserOrderModel(userId, orderItemModelList);
		Mockito.when(orderRepository.findByUserId(userId)).thenReturn(orderItemList);
		assertNotEquals(expected, orderService.getUserOrderModel(userId));
	}

	@Test
	public void addAllOrdersFromUserCartTest() throws OrderException {
		Mockito.when(cartRepository.findByUserId(userId)).thenReturn(Arrays.asList(cartItem));
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(product);
		Mockito.when(orderRepository.save(Mockito.any())).thenReturn(orderItem);
		Mockito.doNothing().when(cartRepository).delete(cartItem);
		orderService.addAllOrdersFromCartForUser(userId);
		Mockito.verify(cartRepository).findByUserId(userId);
		Mockito.verify(productRepository).save(Mockito.any());
		Mockito.verify(orderRepository).save(Mockito.any());
		Mockito.verify(cartRepository).delete(cartItem);
	}

	@Test
	public void addAllOrdersFromUserCartExceptionTest() throws OrderException {
		Long cartItemId = 610L;
		CartItem cartItem = new CartItem(1L, 1L, 1L);
		cartItem.setProduct(product);
		cartItem.setCartItemId(cartItemId);
		Mockito.when(cartRepository.findByUserId(userId)).thenReturn(null);
		assertThrows(OrderException.class, () -> orderService.addAllOrdersFromCartForUser(userId));
	}

}
