package com.epam.ecommerce.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.ecommerce.entity.Category;
import com.epam.ecommerce.entity.CategorySubCategoryRelation;
import com.epam.ecommerce.entity.Product;
import com.epam.ecommerce.exception.CategoryException;
import com.epam.ecommerce.exception.DataAlreadyPresentException;
import com.epam.ecommerce.exception.InvalidDataException;
import com.epam.ecommerce.model.CategoryModel;
import com.epam.ecommerce.repository.CategoryRepository;
import com.epam.ecommerce.repository.CategorySubCategoryRelationRepository;

class CategoryServiceTest {

	@Mock
	private CategoryRepository categoryRepository;
	@Mock
	private CategorySubCategoryRelationRepository categorySubCategoryRelationRepository;
	@InjectMocks
	private CategoryService categoryService;

	private String categoryNameMobile = "Mobiles";
	private String categoryNameHomeAppliances = "Home Applicances";
	private String categoryNameElectronics = "Electronics";
	private String categoryNameFashion = "Fashion";
	private Long categoryIdMobile = 3L;
	private Long categoryIdHomeAppliances = 4L;
	private Long categoryIdElectronics = 1L;
	private Long categoryIdFashion = 2L;

	private Category mobileCategory;
	private Category homeAppliancesCategory;
	private Category electronicsCategory;
	private Category fashionCategory;
	private CategoryModel electronicsCategoryModel;
	private CategoryModel mobileCategoryModel;
	private CategoryModel homeAppliancesCategoryModel;
	private Long productIdIphone = 1L;
	private Product productIphone;
	private Long parentCategoryId = 0L;

	@BeforeEach
	public void initialise() {
		productIphone = new Product("Iphone x", new BigDecimal("1000.00"), 1000L);
		productIphone.setProductId(productIdIphone);

		mobileCategory = new Category(categoryNameMobile);
		mobileCategory.setCategoryId(categoryIdMobile);
		mobileCategory.setCategorySubCategoryRelationList(Arrays.asList());

		mobileCategoryModel = new CategoryModel(categoryIdMobile, categoryNameMobile);

		homeAppliancesCategory = new Category(categoryNameHomeAppliances);
		homeAppliancesCategory.setCategoryId(categoryIdHomeAppliances);
		electronicsCategory = new Category(categoryNameElectronics);
		electronicsCategory.setCategoryId(categoryIdElectronics);
		electronicsCategory.setProductCategoryRelationList(new ArrayList<>());
		electronicsCategory.setCategorySubCategoryRelationList(Arrays.asList());
		fashionCategory = new Category(categoryNameFashion);
		fashionCategory.setCategoryId(categoryIdFashion);
		electronicsCategoryModel = new CategoryModel(categoryIdElectronics, categoryNameElectronics);

		MockitoAnnotations.initMocks(this);
	}

	@Test
	void getSubCategoriesByParentCategoryIdTest() throws CategoryException {
		Long parentCategoryId = 1L;
		Mockito.when(categorySubCategoryRelationRepository.findAllSubCategoryIdsByCategoryId(parentCategoryId))
				.thenReturn(Arrays.asList(categoryIdMobile));
		Mockito.when(categoryRepository.findById(categoryIdMobile)).thenReturn(Optional.ofNullable(mobileCategory));
		List<CategoryModel> categoryModelList = new ArrayList<>();
		categoryModelList.add(mobileCategoryModel);
		assertEquals(categoryModelList, categoryService.getCategoriesByParentCategoryId(parentCategoryId));
	}

	@Test
	void getSubCategoriesByParentCategoryIdTestFailure() throws CategoryException {
		Long parentCategoryId = 1L;
		Mockito.when(categorySubCategoryRelationRepository.findAllSubCategoryIdsByCategoryId(parentCategoryId))
				.thenReturn(Arrays.asList(categoryIdMobile));
		Mockito.when(categoryRepository.findById(categoryIdMobile)).thenReturn(Optional.ofNullable(mobileCategory));
		List<CategoryModel> categoryModelList = new ArrayList<>();
		categoryModelList.add(homeAppliancesCategoryModel);
		assertNotEquals(categoryModelList, categoryService.getCategoriesByParentCategoryId(parentCategoryId));
	}

	@Test
	void getSubCategoriesByParentCategoryIdTestForCategoryException() throws CategoryException {
		Long parentCategoryId = 1L;
		Mockito.when(categorySubCategoryRelationRepository.findAllSubCategoryIdsByCategoryId(parentCategoryId))
				.thenReturn(Arrays.asList(categoryIdMobile));
		Mockito.when(categoryRepository.findById(categoryIdMobile)).thenReturn(Optional.ofNullable(null));
		assertThrows(CategoryException.class, () -> categoryService.getCategoriesByParentCategoryId(parentCategoryId));
	}

	@Test
	void getMainCategoriesByParentCategoryIdTest() throws CategoryException {
		Long parentCategoryId = 0L;
		CategoryModel electronicsCategoryModel = new CategoryModel(categoryIdElectronics, categoryNameElectronics);
		List<CategoryModel> categoryModelList = Arrays.asList(electronicsCategoryModel);
		Mockito.when(categorySubCategoryRelationRepository.findAllSubCategoryIds()).thenReturn(Arrays.asList(2L));
		Mockito.when(categoryRepository.findAllCategoryIds()).thenReturn(Arrays.asList(1L, 2L));
		Mockito.when(categoryRepository.findById(1L)).thenReturn(Optional.ofNullable(electronicsCategory));
		assertEquals(categoryModelList, categoryService.getCategoriesByParentCategoryId(parentCategoryId));
	}

	@Test
	void getMainCategoriesByParentCategoryIdFailureTest() throws CategoryException {
		Long parentCategoryId = 0L;
		CategoryModel electronicsCategoryModel = new CategoryModel(categoryIdElectronics, categoryNameElectronics);
		List<CategoryModel> categoryModelList = Arrays.asList(electronicsCategoryModel);
		Mockito.when(categorySubCategoryRelationRepository.findAllSubCategoryIds()).thenReturn(Arrays.asList(2L));
		Mockito.when(categoryRepository.findAllCategoryIds()).thenReturn(Arrays.asList(1L, 2L));
		Mockito.when(categoryRepository.findById(1L)).thenReturn(Optional.ofNullable(mobileCategory));
		assertNotEquals(categoryModelList, categoryService.getCategoriesByParentCategoryId(parentCategoryId));
	}

	@Test
	void addCategoryUnderMainCategoryTest() throws InvalidDataException, DataAlreadyPresentException {
		Mockito.when(categoryRepository.findByName(categoryNameElectronics)).thenReturn(Optional.ofNullable(null));
		Mockito.when(categoryRepository.save(Mockito.any())).thenReturn(electronicsCategory);
		assertEquals(electronicsCategoryModel, categoryService.addCategory(parentCategoryId, electronicsCategoryModel));
	}

	@Test
	void addCategoryUnderSubCategoryTest() throws InvalidDataException, DataAlreadyPresentException {
		Mockito.when(categoryRepository.findByName(categoryNameMobile)).thenReturn(Optional.ofNullable(null));
		Mockito.when(categoryRepository.save(Mockito.any())).thenReturn(electronicsCategory);
		Mockito.when(categorySubCategoryRelationRepository.save(Mockito.any()))
				.thenReturn(new CategorySubCategoryRelation());
		assertEquals(electronicsCategoryModel, categoryService.addCategory(categoryIdElectronics, mobileCategoryModel));
	}

	@Test
	void addCategoryUnderMainCategoryTestInvalidDataException()
			throws InvalidDataException, DataAlreadyPresentException {
		String invalidCategoryName = "";
		CategoryModel categoryModel = new CategoryModel(categoryIdElectronics, invalidCategoryName);
		Mockito.when(categoryRepository.findByName(categoryNameMobile)).thenReturn(Optional.ofNullable(null));
		Mockito.when(categoryRepository.save(Mockito.any())).thenReturn(electronicsCategory);
		Mockito.when(categorySubCategoryRelationRepository.save(Mockito.any()))
				.thenReturn(new CategorySubCategoryRelation());
		assertThrows(InvalidDataException.class,
				() -> categoryService.addCategory(categoryIdElectronics, categoryModel));
	}

	@Test
	void deleteMainCategoryTest() {
		Mockito.when(categoryRepository.findById(categoryIdElectronics))
				.thenReturn(Optional.ofNullable(electronicsCategory));
		Mockito.when(categoryRepository.save(Mockito.any())).thenReturn(electronicsCategory);
		assertEquals(electronicsCategoryModel, categoryService.deleteCategory(parentCategoryId, categoryIdElectronics));
	}

	@Test
	void deleteMainCategoryTestCategoryException() {
		Mockito.when(categoryRepository.findById(categoryIdElectronics)).thenReturn(Optional.ofNullable(null));
		Mockito.when(categoryRepository.save(Mockito.any())).thenReturn(electronicsCategory);
		assertThrows(CategoryException.class,
				() -> categoryService.deleteCategory(parentCategoryId, categoryIdElectronics));
	}

	@Test
	void deleteSubCategoryTest() {
		Mockito.when(categoryRepository.findById(categoryIdMobile)).thenReturn(Optional.ofNullable(mobileCategory));
		Mockito.when(categorySubCategoryRelationRepository.save(Mockito.any()))
				.thenReturn(new CategorySubCategoryRelation());
		Mockito.when(categoryRepository.save(Mockito.any())).thenReturn(electronicsCategory);
		assertEquals(mobileCategoryModel, categoryService.deleteCategory(categoryIdElectronics, categoryIdMobile));
	}

}
