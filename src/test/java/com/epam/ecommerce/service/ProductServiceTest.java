package com.epam.ecommerce.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.ecommerce.entity.Product;
import com.epam.ecommerce.entity.ProductCategoryRelation;
import com.epam.ecommerce.exception.CategoryException;
import com.epam.ecommerce.exception.DataAlreadyPresentException;
import com.epam.ecommerce.exception.InvalidDataException;
import com.epam.ecommerce.exception.ProductException;
import com.epam.ecommerce.model.ProductModel;
import com.epam.ecommerce.repository.CategoryRepository;
import com.epam.ecommerce.repository.ProductCategoryRelationRepository;
import com.epam.ecommerce.repository.ProductRepository;

class ProductServiceTest {

	@Mock
	private ProductRepository productRepository;
	@Mock
	private ProductCategoryRelationRepository productCategoryRelationRepository;
	@Mock
	private CategoryRepository categoryRepository;
	@InjectMocks
	private ProductService productService;

	private Long categoryId = 5L;
	private String productNameShirt = "Raymond Shirt";
	private String productNameJeans = "Wrangler Jeans";
	private Long productQuantityShirt = 100L;
	private Long productQuantityJeans = 100L;
	private BigDecimal productPriceShirt = new BigDecimal("50.00");
	private BigDecimal productPriceJeans = new BigDecimal("50.00");
	private Long productIdJeans = 7L;
	private Long productIdShirt = 8L;
	private Long productCategoryRelationIdForJeans = 7L;
	private Long productCategoryRelationIdForShirt = 8L;

	private Product jeansProduct;
	private Product shirtProduct;
	private ProductModel jeansProductModel;
	private ProductModel shirtProductModel;
	private ProductCategoryRelation productCategoryRelationForJeans;
	private ProductCategoryRelation productCategoryRelationForShirt;

	@BeforeEach
	public void initialise() throws ClassNotFoundException {
		jeansProduct = new Product(productNameJeans, productPriceJeans, productQuantityJeans);
		shirtProduct = new Product(productNameShirt, productPriceShirt, productQuantityShirt);
		jeansProduct.setProductId(productIdJeans);
		shirtProduct.setProductId(productIdShirt);
		jeansProductModel = new ProductModel(productIdJeans, productNameJeans, productQuantityJeans, productPriceJeans);
		shirtProductModel = new ProductModel(productIdShirt, productNameShirt, productQuantityShirt, productPriceShirt);
		productCategoryRelationForJeans = new ProductCategoryRelation(productIdJeans, categoryId);
		productCategoryRelationForJeans.setProductCategoryRelationId(productCategoryRelationIdForJeans);
		productCategoryRelationForJeans.setProduct(jeansProduct);
		productCategoryRelationForShirt = new ProductCategoryRelation(productIdShirt, categoryId);
		productCategoryRelationForShirt.setProductCategoryRelationId(productCategoryRelationIdForShirt);
		productCategoryRelationForShirt.setProduct(shirtProduct);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getProductsUnderCategoryTest() throws ProductException {
		List<ProductModel> expected = Arrays.asList(jeansProductModel, shirtProductModel);
		List<ProductCategoryRelation> productCategoryRelationsList = Arrays.asList(productCategoryRelationForJeans,
				productCategoryRelationForShirt);
		Mockito.when(productCategoryRelationRepository.findByCategoryId(categoryId))
				.thenReturn(productCategoryRelationsList);
		assertEquals(expected, productService.getProductsUnderCategory(categoryId));
	}

	@Test
	public void getProductsUnderCategoryFailureTest() throws ProductException {
		List<ProductModel> expected = Arrays.asList(jeansProductModel, shirtProductModel);
		List<ProductCategoryRelation> productCategoryRelationsList = Arrays.asList(productCategoryRelationForShirt,
				productCategoryRelationForShirt);
		Mockito.when(productCategoryRelationRepository.findByCategoryId(categoryId))
				.thenReturn(productCategoryRelationsList);
		assertNotEquals(expected, productService.getProductsUnderCategory(categoryId));
	}

	@Test
	public void addProductTest() throws CategoryException, InvalidDataException, DataAlreadyPresentException {
		Mockito.when(categoryRepository.existsById(categoryId)).thenReturn(Boolean.TRUE);
		Mockito.when(productRepository.findByName(productNameJeans)).thenReturn(Optional.ofNullable(null));
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(jeansProduct);
		Mockito.when(productCategoryRelationRepository.save(Mockito.any())).thenReturn(productCategoryRelationForJeans);
		assertEquals(jeansProductModel, productService.addProduct(categoryId, jeansProductModel));
	}

	@Test
	public void addProductTestCategoryException()
			throws CategoryException, InvalidDataException, DataAlreadyPresentException {
		Mockito.when(categoryRepository.existsById(categoryId)).thenReturn(Boolean.FALSE);
		Mockito.when(productRepository.findByName(productNameJeans)).thenReturn(Optional.ofNullable(null));
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(jeansProduct);
		Mockito.when(productCategoryRelationRepository.save(Mockito.any())).thenReturn(productCategoryRelationForJeans);
		assertThrows(CategoryException.class, () -> productService.addProduct(categoryId, jeansProductModel));
	}

	@Test
	public void addProductTestDataAlreadyPresentException()
			throws CategoryException, InvalidDataException, DataAlreadyPresentException {
		ProductModel productModelAlreadyPresent = new ProductModel(null, productNameJeans, productQuantityJeans,
				productPriceJeans);
		Mockito.when(categoryRepository.existsById(categoryId)).thenReturn(Boolean.TRUE);
		Mockito.when(productRepository.findByName(productNameJeans)).thenReturn(Optional.ofNullable(jeansProduct));
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(jeansProduct);
		Mockito.when(productCategoryRelationRepository.save(Mockito.any())).thenReturn(productCategoryRelationForJeans);
		assertThrows(DataAlreadyPresentException.class,
				() -> productService.addProduct(categoryId, productModelAlreadyPresent));
	}

	@Test
	public void addProductTestInvalidDataException()
			throws CategoryException, InvalidDataException, DataAlreadyPresentException {
		String inValidProductName = "";
		ProductModel productModelWithInvalidData = new ProductModel(productIdJeans, inValidProductName,
				productQuantityJeans, productPriceJeans);
		Product productWithInvalidData = new Product(inValidProductName, productPriceJeans, productQuantityJeans);
		Mockito.when(categoryRepository.existsById(categoryId)).thenReturn(Boolean.TRUE);
		Mockito.when(productRepository.findByName(productNameJeans))
				.thenReturn(Optional.ofNullable(productWithInvalidData));
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(jeansProduct);
		Mockito.when(productCategoryRelationRepository.save(Mockito.any())).thenReturn(productCategoryRelationForJeans);
		assertThrows(InvalidDataException.class,
				() -> productService.addProduct(categoryId, productModelWithInvalidData));
	}

	@Test
	public void deleteProductTest() throws ProductException {
		Mockito.when(productRepository.findById(productIdJeans)).thenReturn(Optional.ofNullable(jeansProduct));
		Mockito.doNothing().when(productCategoryRelationRepository).deleteByProductId(productIdJeans);
		assertEquals(jeansProductModel, productService.deleteProductById(productIdJeans));
	}

	@Test
	public void deleteProductTestProductException() throws ProductException {
		Mockito.when(productRepository.findById(productIdJeans)).thenReturn(Optional.ofNullable(null));
		Mockito.doNothing().when(productCategoryRelationRepository).deleteByProductId(productIdJeans);
		assertThrows(ProductException.class, () -> productService.deleteProductById(productIdJeans));
	}

	@Test
	public void updateProductTest()
			throws CategoryException, ProductException, InvalidDataException, DataAlreadyPresentException {
		Mockito.when(productRepository.findById(productIdJeans)).thenReturn(Optional.ofNullable(jeansProduct));
		Mockito.when(categoryRepository.existsById(categoryId)).thenReturn(Boolean.TRUE);
		Mockito.when(productRepository.findByName(productNameJeans)).thenReturn(Optional.ofNullable(jeansProduct));
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(jeansProduct);
		assertEquals(jeansProductModel, productService.updateProduct(categoryId, productIdJeans, jeansProductModel));
	}

	@Test
	public void updateProductTestProductException()
			throws CategoryException, ProductException, InvalidDataException, DataAlreadyPresentException {
		Mockito.when(productRepository.findById(productIdJeans)).thenReturn(Optional.ofNullable(null));
		Mockito.when(categoryRepository.existsById(categoryId)).thenReturn(Boolean.TRUE);
		Mockito.when(productRepository.findByName(productNameJeans)).thenReturn(Optional.ofNullable(jeansProduct));
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(jeansProduct);
		assertThrows(ProductException.class,
				() -> productService.updateProduct(categoryId, productIdJeans, jeansProductModel));
	}

	@Test
	public void updateProductTestCategoryException()
			throws CategoryException, InvalidDataException, DataAlreadyPresentException {
		Mockito.when(productRepository.findById(productIdJeans)).thenReturn(Optional.ofNullable(jeansProduct));
		Mockito.when(categoryRepository.existsById(categoryId)).thenReturn(Boolean.FALSE);
		Mockito.when(productRepository.findByName(productNameJeans)).thenReturn(Optional.ofNullable(null));
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(jeansProduct);
		assertThrows(CategoryException.class,
				() -> productService.updateProduct(categoryId, productIdJeans, jeansProductModel));
	}

	@Test
	public void updateProductTestDataAlreadyPresentException()
			throws CategoryException, InvalidDataException, DataAlreadyPresentException {
		ProductModel productModelAlreadyPresent = new ProductModel(null, productNameJeans, productQuantityJeans,
				productPriceJeans);
		Mockito.when(productRepository.findById(productIdJeans)).thenReturn(Optional.ofNullable(jeansProduct));
		Mockito.when(categoryRepository.existsById(categoryId)).thenReturn(Boolean.TRUE);
		Mockito.when(productRepository.findByName(productNameJeans)).thenReturn(Optional.ofNullable(jeansProduct));
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(jeansProduct);
		assertThrows(DataAlreadyPresentException.class,
				() -> productService.updateProduct(categoryId, productIdJeans, productModelAlreadyPresent));
	}

	@Test
	public void updateProductTestInvalidDataException()
			throws CategoryException, InvalidDataException, DataAlreadyPresentException {
		String inValidProductName = "";
		ProductModel productModelWithInvalidData = new ProductModel(productIdJeans, inValidProductName,
				productQuantityJeans, productPriceJeans);
		Product productWithInvalidData = new Product(inValidProductName, productPriceJeans, productQuantityJeans);
		Mockito.when(productRepository.findById(productIdJeans)).thenReturn(Optional.ofNullable(jeansProduct));
		Mockito.when(categoryRepository.existsById(categoryId)).thenReturn(Boolean.TRUE);
		Mockito.when(productRepository.findByName(productNameJeans))
				.thenReturn(Optional.ofNullable(productWithInvalidData));
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(jeansProduct);
		assertThrows(InvalidDataException.class,
				() -> productService.updateProduct(categoryId, productIdJeans, productModelWithInvalidData));
	}

}
