package com.epam.ecommerce.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.ecommerce.entity.CartItem;
import com.epam.ecommerce.entity.Product;
import com.epam.ecommerce.exception.CartException;
import com.epam.ecommerce.exception.InsufficientQuantityException;
import com.epam.ecommerce.exception.InvalidQuantityException;
import com.epam.ecommerce.exception.ProductException;
import com.epam.ecommerce.model.CartItemModel;
import com.epam.ecommerce.model.ProductModel;
import com.epam.ecommerce.model.UserCartItemModel;
import com.epam.ecommerce.model.UserCartModel;
import com.epam.ecommerce.repository.CartRepository;
import com.epam.ecommerce.repository.ProductRepository;

class CartServiceTest {

	@Mock
	private CartRepository cartRepository;
	@Mock
	private ProductRepository productRepository;

	@InjectMocks
	private CartService cartService;

	private Long userId = 1L;
	private Long productId = 1L;
	private Long quantity = 1L;
	private Long cartItemId = 1L;
	private String productName = "Iphone x";
	private BigDecimal productPrice = new BigDecimal("1000");
	private Long totalUnits = 100L;
	private CartItem cartItem;
	private Product product;
	private ProductModel productModel;
	private CartItemModel cartItemModel;

	@BeforeEach
	public void initialise() {
		productPrice = productPrice.setScale(2);
		product = new Product(productName, productPrice, totalUnits);
		product.setProductId(productId);
		productModel = new ProductModel(productId, productName, totalUnits, productPrice);
		cartItem = new CartItem(userId, productId, quantity);
		cartItem.setCartItemId(cartItemId);
		cartItem.setProduct(product);
		cartItemModel = new CartItemModel(cartItemId, userId, productId, quantity, productModel);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void addItemToCartTest()
			throws CartException, ProductException, InvalidQuantityException, InsufficientQuantityException {
		CartItemModel cartItemModelToBeAdded = new CartItemModel();
		cartItemModelToBeAdded.setUserId(userId);
		cartItemModelToBeAdded.setProductId(productId);
		cartItemModelToBeAdded.setQuantity(quantity);
		Mockito.when(productRepository.findById(productId)).thenReturn(Optional.ofNullable(product));
		Mockito.when(cartRepository.findByUserIdAndProductId(cartItemModelToBeAdded.getUserId(),
				cartItemModelToBeAdded.getProductId())).thenReturn(null);
		Mockito.when(cartRepository.save(Mockito.any())).thenReturn(cartItem);
		assertEquals(cartItemModel, cartService.addItemToCart(userId, cartItemModelToBeAdded));
	}

	@Test
	public void addItemToCartFailureTest()
			throws CartException, ProductException, InvalidQuantityException, InsufficientQuantityException {
		CartItem cartItemWrong = new CartItem(userId, productId, quantity);
		Long cartItemIdWrong = 2L;
		cartItemWrong.setCartItemId(cartItemIdWrong);
		cartItemWrong.setProduct(product);
		CartItemModel cartItemModelToBeAdded = new CartItemModel();
		cartItemModelToBeAdded.setUserId(userId);
		cartItemModelToBeAdded.setProductId(productId);
		cartItemModelToBeAdded.setQuantity(quantity);
		Mockito.when(productRepository.findById(productId)).thenReturn(Optional.ofNullable(product));
		Mockito.when(cartRepository.findByUserIdAndProductId(cartItemModelToBeAdded.getUserId(),
				cartItemModelToBeAdded.getProductId())).thenReturn(null);
		Mockito.when(cartRepository.save(Mockito.any())).thenReturn(cartItemWrong);
		assertNotEquals(cartItemModel, cartService.addItemToCart(userId, cartItemModelToBeAdded));
	}

	@Test
	public void addItemToCartImproperQuantityExceptionTest()
			throws CartException, ProductException, InvalidQuantityException, InsufficientQuantityException {
		CartItemModel cartItemModelToBeAdded = new CartItemModel();
		cartItemModelToBeAdded.setUserId(userId);
		cartItemModelToBeAdded.setProductId(productId);
		cartItemModelToBeAdded.setQuantity(0L);
		assertThrows(InvalidQuantityException.class, () -> cartService.addItemToCart(userId, cartItemModelToBeAdded));
	}

	@Test
	public void addItemToCartTestForProductException() {
		CartItemModel cartItemModelToBeAdded = new CartItemModel();
		cartItemModelToBeAdded.setUserId(userId);
		cartItemModelToBeAdded.setProductId(productId);
		cartItemModelToBeAdded.setQuantity(quantity);
		Mockito.when(productRepository.findById(productId)).thenReturn(Optional.ofNullable(null));
		assertThrows(ProductException.class, () -> cartService.addItemToCart(userId, cartItemModelToBeAdded));
	}

	@Test
	public void addItemToCartInsufficientQuantityTest() {
		CartItemModel cartItemModelToBeAdded = new CartItemModel();
		cartItemModelToBeAdded.setUserId(userId);
		cartItemModelToBeAdded.setProductId(productId);
		cartItemModelToBeAdded.setQuantity(101L);
		Mockito.when(productRepository.findById(productId)).thenReturn(Optional.ofNullable(product));
		Mockito.when(cartRepository.findByUserIdAndProductId(cartItemModelToBeAdded.getUserId(),
				cartItemModelToBeAdded.getProductId())).thenReturn(null);
		Mockito.when(cartRepository.save(Mockito.any())).thenReturn(cartItem);
		assertThrows(InsufficientQuantityException.class,
				() -> cartService.addItemToCart(userId, cartItemModelToBeAdded));
	}

	@Test
	public void editItemInCartTest()
			throws CartException, ProductException, InvalidQuantityException, InsufficientQuantityException {
		CartItemModel cartItemModelToBeAdded = new CartItemModel();
		cartItemModelToBeAdded.setUserId(userId);
		cartItemModelToBeAdded.setProductId(productId);
		cartItemModelToBeAdded.setQuantity(quantity);
		cartItemModel.setQuantity(quantity + quantity);
		Mockito.when(productRepository.findById(productId)).thenReturn(Optional.ofNullable(product));
		Mockito.when(cartRepository.findByUserIdAndProductId(cartItemModelToBeAdded.getUserId(),
				cartItemModelToBeAdded.getProductId())).thenReturn(cartItem);
		Mockito.when(cartRepository.save(Mockito.any())).thenReturn(cartItem);
		assertEquals(cartItemModel, cartService.addItemToCart(userId, cartItemModelToBeAdded));
	}

	@Test
	public void editItemInCartInsufficientQuantityTest()
			throws CartException, ProductException, InvalidQuantityException, InsufficientQuantityException {
		CartItemModel cartItemModelToBeAdded = new CartItemModel();
		cartItemModelToBeAdded.setUserId(userId);
		cartItemModelToBeAdded.setProductId(productId);
		cartItemModelToBeAdded.setQuantity(101L);
		Mockito.when(productRepository.findById(productId)).thenReturn(Optional.ofNullable(product));
		Mockito.when(cartRepository.findByUserIdAndProductId(cartItemModelToBeAdded.getUserId(),
				cartItemModelToBeAdded.getProductId())).thenReturn(cartItem);
		Mockito.when(cartRepository.save(Mockito.any())).thenReturn(cartItem);
		assertThrows(InsufficientQuantityException.class,
				() -> cartService.addItemToCart(userId, cartItemModelToBeAdded));
	}

	@Test
	public void removeItemFromCartTest() throws CartException {
		Mockito.when(cartRepository.findByCartItemIdAndUserId(cartItemId, userId))
				.thenReturn(Optional.ofNullable(cartItem));
		Mockito.doNothing().when(cartRepository).delete(cartItem);
		assertEquals(cartItemModel, cartService.removeItemFromCart(userId, cartItemId));
	}

	@Test
	public void removeItemFromCartFailureTest() throws CartException {
		Long quantityWrong = 2L;
		CartItemModel cartItemModelWrong = new CartItemModel(cartItemId, userId, productId, quantityWrong,
				productModel);
		Mockito.when(cartRepository.findByCartItemIdAndUserId(cartItemId, userId))
				.thenReturn(Optional.ofNullable(cartItem));
		Mockito.doNothing().when(cartRepository).delete(cartItem);
		assertNotEquals(cartItemModelWrong, cartService.removeItemFromCart(userId, cartItemId));
	}

	@Test
	public void removeItemFromCartExceptionTestForItemNotFound() {
		Mockito.when(cartRepository.findByCartItemIdAndUserId(cartItemId, userId))
				.thenReturn(Optional.ofNullable(null));
		assertThrows(CartException.class, () -> cartService.removeItemFromCart(userId, cartItemId));
	}

	@Test
	public void getUserCartModelTest() throws ProductException, CartException {
		UserCartItemModel userCartItemModel = new UserCartItemModel(cartItem.getCartItemId(), productModel,
				cartItem.getQuantity());
		List<UserCartItemModel> userCartItemModelList = new ArrayList<>();
		userCartItemModelList.add(userCartItemModel);
		UserCartModel expected = new UserCartModel(userId, userCartItemModelList, productPrice);
		List<CartItem> cartItemList = new ArrayList<>();
		cartItemList.add(cartItem);
		Mockito.when(cartRepository.findByUserId(userId)).thenReturn(cartItemList);
		assertEquals(expected, cartService.getUserCartModel(userId));
	}

	@Test
	public void getUserCartModelFailureTest() throws ProductException, CartException {
		Long wrongQuantity = 2L;
		UserCartItemModel userCartItemModel = new UserCartItemModel(cartItem.getCartItemId(), productModel,
				wrongQuantity);
		List<UserCartItemModel> userCartItemModelList = new ArrayList<>();
		userCartItemModelList.add(userCartItemModel);
		UserCartModel expected = new UserCartModel(userId, userCartItemModelList, productPrice);
		List<CartItem> cartItemList = new ArrayList<>();
		cartItemList.add(cartItem);
		Mockito.when(cartRepository.findByUserId(userId)).thenReturn(cartItemList);
		assertNotEquals(expected, cartService.getUserCartModel(userId));
	}

	@Test
	public void editCartItemQuantityTest()
			throws CartException, InsufficientQuantityException, InvalidQuantityException {
		Long updatedQuantity = 10L;
		CartItem cartItemAdded = new CartItem(userId, productId, updatedQuantity);
		cartItemAdded.setCartItemId(cartItemId);
		cartItemAdded.setProduct(product);
		CartItemModel cartItemModelToBeEdited = new CartItemModel(cartItemId, userId, productId, updatedQuantity,
				productModel);
		cartItemModel.setQuantity(updatedQuantity);
		Mockito.when(cartRepository.findByCartItemIdAndUserId(cartItemId, userId))
				.thenReturn(Optional.ofNullable(cartItem));
		Mockito.when(cartRepository.save(Mockito.any())).thenReturn(cartItemAdded);
		assertEquals(cartItemModel, cartService.editCartItemQuantity(userId, cartItemModelToBeEdited));
	}

	@Test
	public void editCartItemQuantityInvalidQuantityExceptionTest()
			throws CartException, InsufficientQuantityException, InvalidQuantityException {
		Long invalidQuantity = 0L;
		CartItem cartItemAdded = new CartItem(userId, productId, quantity);
		cartItemAdded.setCartItemId(cartItemId);
		cartItemAdded.setProduct(product);
		cartItemModel.setQuantity(invalidQuantity);
		Mockito.when(cartRepository.findByCartItemIdAndUserId(cartItemId, userId))
				.thenReturn(Optional.ofNullable(cartItem));
		Mockito.when(cartRepository.save(Mockito.any())).thenReturn(cartItemAdded);
		assertThrows(InvalidQuantityException.class, () -> cartService.editCartItemQuantity(userId, cartItemModel));
	}

	@Test
	public void editCartItemQuantityInsufficientQuantityExceptionTest()
			throws CartException, InsufficientQuantityException, InvalidQuantityException {
		Long insufficientQuantity = 101L;
		CartItem cartItemAdded = new CartItem(userId, productId, quantity);
		cartItemAdded.setCartItemId(cartItemId);
		cartItemAdded.setProduct(product);
		cartItemModel.setQuantity(insufficientQuantity);
		Mockito.when(cartRepository.findByCartItemIdAndUserId(cartItemId, userId))
				.thenReturn(Optional.ofNullable(cartItem));
		Mockito.when(cartRepository.save(Mockito.any())).thenReturn(cartItemAdded);
		assertThrows(InsufficientQuantityException.class,
				() -> cartService.editCartItemQuantity(userId, cartItemModel));
	}

	@Test
	public void editCartItemQuantityCartExceptionTest()
			throws CartException, InsufficientQuantityException, InvalidQuantityException {

		Mockito.when(cartRepository.findByCartItemIdAndUserId(cartItemId, userId))
				.thenReturn(Optional.ofNullable(null));
		assertThrows(CartException.class, () -> cartService.editCartItemQuantity(userId, cartItemModel));
	}

}
