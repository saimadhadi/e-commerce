package com.epam.ecommerce.restcontroller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.epam.ecommerce.exception.CategoryException;
import com.epam.ecommerce.exception.DataAlreadyPresentException;
import com.epam.ecommerce.exception.InvalidDataException;
import com.epam.ecommerce.exception.ProductException;
import com.epam.ecommerce.model.EcommerceResponse;
import com.epam.ecommerce.model.EcommerceResponse.ECommerceStatus;
import com.epam.ecommerce.model.ProductModel;
import com.epam.ecommerce.service.ProductService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ProductRestControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Mock
	private ProductService productService;

	@InjectMocks
	private ProductRestController productRestController;

	private ObjectMapper mapper = new ObjectMapper();
	private Long productId = 1L;
	private String productName = "iPhone X";
	private Long quantity = 100L;
	private BigDecimal price = new BigDecimal("100");

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(productRestController).build();
	}

	@Test
	public void getProductListTest() throws Exception {
		Long categoryId = 3L;
		List<ProductModel> productModelList = new ArrayList<>();
		productModelList.add(new ProductModel(productId, productName, quantity, price));
		Mockito.when(productService.getProductsUnderCategory(categoryId)).thenReturn(productModelList);
		this.mockMvc.perform(get("/rest/category/3/product")).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(productModelList)));
	}

	@Test
	public void updateProductUnderCategory() throws JsonProcessingException, Exception {
		ProductModel productModel = new ProductModel(productId, productName, quantity, price);
		EcommerceResponse<ProductModel> response = new EcommerceResponse<>(productModel, ECommerceStatus.Success);
		Mockito.when(productService.updateProduct(Mockito.anyLong(), Mockito.any(), Mockito.any()))
				.thenReturn(productModel);
		this.mockMvc
				.perform(put("/rest/category/3/product/1").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(productModel)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(response)));
	}

	@Test
	public void updateProductUnderCategoryProductException() throws JsonProcessingException, Exception {
		ProductModel productModel = new ProductModel(productId, productName, quantity, price);
		Mockito.when(productService.updateProduct(Mockito.anyLong(), Mockito.any(), Mockito.any()))
				.thenThrow(ProductException.class);
		this.mockMvc.perform(put("/rest/category/3/product/1").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(productModel))).andExpect(status().is4xxClientError());
	}

	@Test
	public void updateProductUnderCategoryInvalidDataException() throws JsonProcessingException, Exception {
		ProductModel productModel = new ProductModel(productId, productName, quantity, price);
		EcommerceResponse<ProductModel> response = new EcommerceResponse<>(ECommerceStatus.InvalidData);
		Mockito.when(productService.updateProduct(Mockito.anyLong(), Mockito.any(), Mockito.any()))
				.thenThrow(InvalidDataException.class);
		this.mockMvc
				.perform(put("/rest/category/3/product/1").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(productModel)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(response)));
	}

	@Test
	public void updateProductUnderCategoryDataAlreadyPresentException() throws JsonProcessingException, Exception {
		ProductModel productModel = new ProductModel(productId, productName, quantity, price);
		EcommerceResponse<ProductModel> response = new EcommerceResponse<>(ECommerceStatus.AlreadyPresent);
		Mockito.when(productService.updateProduct(Mockito.anyLong(), Mockito.any(), Mockito.any()))
				.thenThrow(DataAlreadyPresentException.class);
		this.mockMvc
				.perform(put("/rest/category/3/product/1").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(productModel)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(response)));
	}

	@Test
	public void updateProductUnderCategoryCategoryNotFoundException() throws JsonProcessingException, Exception {
		ProductModel productModel = new ProductModel(productId, productName, quantity, price);
		Mockito.when(productService.updateProduct(Mockito.anyLong(), Mockito.any(), Mockito.any()))
				.thenThrow(CategoryException.class);
		this.mockMvc.perform(put("/rest/category/3/product/1").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(productModel))).andExpect(status().is4xxClientError());
	}

	@Test
	public void addProductUnderCategory() throws JsonProcessingException, Exception {
		ProductModel productModel = new ProductModel(productId, productName, quantity, price);
		EcommerceResponse<ProductModel> response = new EcommerceResponse<>(productModel, ECommerceStatus.Success);
		Mockito.when(productService.addProduct(Mockito.anyLong(), Mockito.any())).thenReturn(productModel);
		this.mockMvc
				.perform(post("/rest/category/3/product").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(productModel)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(response)));
	}

	@Test
	public void addProductUnderCategoryInvalidDataException() throws JsonProcessingException, Exception {
		ProductModel productModel = new ProductModel(productId, productName, quantity, price);
		EcommerceResponse<ProductModel> response = new EcommerceResponse<>(ECommerceStatus.InvalidData);
		Mockito.when(productService.addProduct(Mockito.anyLong(), Mockito.any())).thenThrow(InvalidDataException.class);
		this.mockMvc
				.perform(post("/rest/category/3/product").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(productModel)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(response)));
	}

	@Test
	public void addProductUnderCategoryDataAlreadyPresentException() throws JsonProcessingException, Exception {
		ProductModel productModel = new ProductModel(productId, productName, quantity, price);
		EcommerceResponse<ProductModel> response = new EcommerceResponse<>(ECommerceStatus.AlreadyPresent);
		Mockito.when(productService.addProduct(Mockito.anyLong(), Mockito.any()))
				.thenThrow(DataAlreadyPresentException.class);
		this.mockMvc
				.perform(post("/rest/category/3/product").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(productModel)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(response)));
	}

	@Test
	public void addProductUnderCategoryCategoryNotFoundException() throws JsonProcessingException, Exception {
		ProductModel productModel = new ProductModel(productId, productName, quantity, price);
		Mockito.when(productService.addProduct(Mockito.anyLong(), Mockito.any())).thenThrow(CategoryException.class);
		this.mockMvc.perform(post("/rest/category/3/product").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(productModel))).andExpect(status().is4xxClientError());
	}

	@Test
	public void deleteProductTestProductException() throws JsonProcessingException, Exception {
		ProductModel productModel = new ProductModel(productId, productName, quantity, price);
		Mockito.when(productService.deleteProductById(productId)).thenThrow(ProductException.class);
		this.mockMvc.perform(delete("/rest/category/3/product/1").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(productModel))).andExpect(status().is4xxClientError());
	}

	@Test
	public void deleteProductTest() throws JsonProcessingException, Exception {
		ProductModel productModel = new ProductModel(productId, productName, quantity, price);
		Mockito.when(productService.deleteProductById(productId)).thenReturn(productModel);
		this.mockMvc.perform(delete("/rest/category/3/product/1").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(productModel))).andExpect(status().is2xxSuccessful());
	}
}
