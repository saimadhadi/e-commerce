package com.epam.ecommerce.restcontroller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.epam.ecommerce.exception.CartException;
import com.epam.ecommerce.exception.InsufficientQuantityException;
import com.epam.ecommerce.exception.OrderException;
import com.epam.ecommerce.model.CartItemModel;
import com.epam.ecommerce.model.EcommerceResponse;
import com.epam.ecommerce.model.EcommerceResponse.ECommerceStatus;
import com.epam.ecommerce.model.OrderItemModel;
import com.epam.ecommerce.model.UserOrderModel;
import com.epam.ecommerce.service.OrderService;
import com.fasterxml.jackson.databind.ObjectMapper;

public class OrderRestControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Mock
	private OrderService orderService;

	@InjectMocks
	private OrderRestController orderRestController;

	private ObjectMapper mapper = new ObjectMapper();
	private Long userId = 1L;
	private Long orderItemId = 1L;
	private String productName = "Iphone x";
	private BigDecimal orderItemPrice = new BigDecimal("1000.00");;
	private Long quantity = 1L;
	private Boolean isDelivered = Boolean.FALSE;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(orderRestController).build();
	}

	@Test
	public void getUserOrdersTest() throws Exception {
		UserOrderModel userOrderModel = new UserOrderModel(1L, new ArrayList<>());
		Mockito.when(orderService.getUserOrderModel(1L)).thenReturn(userOrderModel);
		this.mockMvc.perform(get("/rest/user/1/orders")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(userOrderModel)));

	}

	@Test
	public void addOrderItemForUserTest() throws Exception {
		OrderItemModel orderItemModel = new OrderItemModel(orderItemId, userId, productName, orderItemPrice, quantity,
				isDelivered);
		Long cartItemId = 1L;
		Mockito.when(orderService.addOrder(userId, cartItemId)).thenReturn(orderItemModel);
		CartItemModel cartItemModel = new CartItemModel();
		cartItemModel.setCartItemId(cartItemId);
		EcommerceResponse<OrderItemModel> response = new EcommerceResponse<>(orderItemModel, ECommerceStatus.Success);
		this.mockMvc
				.perform(post("/rest/user/1/orders").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel)))
				.andExpect(status().is2xxSuccessful()).andExpect(content().json(mapper.writeValueAsString(response)));

	}

	@Test
	public void addOrderItemForUserInsufficientQuantityExceptionTest() throws Exception {
		Long cartItemId = 1L;
		Long userId = 1L;
		CartItemModel cartItemModel = new CartItemModel();
		cartItemModel.setCartItemId(cartItemId);
		Mockito.when(orderService.addOrder(userId, cartItemId)).thenThrow(InsufficientQuantityException.class);
		EcommerceResponse<OrderItemModel> response = new EcommerceResponse<>(ECommerceStatus.InsufficientStock);
		this.mockMvc
				.perform(post("/rest/user/1/orders").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel)))
				.andExpect(status().isOk()).andExpect(content().json(mapper.writeValueAsString(response)));
	}

	@Test
	public void addOrderItemForUserOrderExceptionTest() throws Exception {
		Long cartItemId = 1L;
		Mockito.when(orderService.addOrder(userId, cartItemId)).thenThrow(CartException.class);
		CartItemModel cartItemModel = new CartItemModel();
		cartItemModel.setCartItemId(cartItemId);
		this.mockMvc.perform(post("/rest/user/1/orders").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(cartItemModel))).andExpect(status().is4xxClientError());
	}

	@Test
	public void addAllOrdersFromCartForUserTest() throws Exception {
		Long userId = 1L;
		Mockito.doNothing().when(orderService).addAllOrdersFromCartForUser(userId);
		this.mockMvc.perform(post("/rest/user/1/orders/all")).andExpect(status().is2xxSuccessful())
				.andExpect(content().string("true"));
	}

	@Test
	public void addAllOrdersFromCartForUserExceptionTest() throws Exception {
		Long userId = 1L;
		Mockito.doThrow(OrderException.class).when(orderService).addAllOrdersFromCartForUser(userId);
		this.mockMvc.perform(post("/rest/user/1/orders/all")).andExpect(status().is2xxSuccessful());
	}
}
