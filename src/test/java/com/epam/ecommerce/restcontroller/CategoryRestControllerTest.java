package com.epam.ecommerce.restcontroller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.epam.ecommerce.exception.CategoryException;
import com.epam.ecommerce.exception.DataAlreadyPresentException;
import com.epam.ecommerce.exception.InvalidDataException;
import com.epam.ecommerce.model.CategoryModel;
import com.epam.ecommerce.model.EcommerceResponse;
import com.epam.ecommerce.model.EcommerceResponse.ECommerceStatus;
import com.epam.ecommerce.service.CategoryService;
import com.epam.ecommerce.service.ProductService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

class CategoryRestControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Mock
	private CategoryService categoryService;

	@Mock
	private ProductService productService;

	@InjectMocks
	private CategoryRestController categoryRestController;

	private ObjectMapper mapper = new ObjectMapper();
	private Long categoryId = 1L;
	private Long parentCategoryId = 0L;
	private String categoryName = "Electronics";

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(categoryRestController).build();
	}

	@Test
	public void getCategoryListTest() throws Exception {
		Long categoryId = 0L;
		CategoryModel categoryModelElectronics = new CategoryModel(1L, "Electronics");
		List<CategoryModel> categoryModelList = new ArrayList<>();
		categoryModelList.add(categoryModelElectronics);
		Mockito.when(categoryService.getCategoriesByParentCategoryId(categoryId)).thenReturn(categoryModelList);
		this.mockMvc.perform(get("/rest/category/0/sub-category")).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(categoryModelList)));
	}

	@Test
	public void addCategoryTest() throws JsonProcessingException, Exception {
		CategoryModel categoryModel = new CategoryModel(categoryId, categoryName);
		EcommerceResponse<CategoryModel> response = new EcommerceResponse<>(categoryModel, ECommerceStatus.Success);
		Mockito.when(categoryService.addCategory(parentCategoryId, categoryModel)).thenReturn(categoryModel);
		this.mockMvc
				.perform(post("/rest/category/0/sub-category").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(categoryModel)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(response)));
	}

	@Test
	public void addCategoryTestInvalidDataException() throws JsonProcessingException, Exception {
		CategoryModel categoryModel = new CategoryModel(categoryId, categoryName);
		EcommerceResponse<CategoryModel> response = new EcommerceResponse<>(ECommerceStatus.InvalidData);
		Mockito.when(categoryService.addCategory(parentCategoryId, categoryModel))
				.thenThrow(InvalidDataException.class);
		this.mockMvc
				.perform(post("/rest/category/0/sub-category").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(categoryModel)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(response)));
	}

	@Test
	public void addCategoryTestDataAlreadyPresentException() throws JsonProcessingException, Exception {
		CategoryModel categoryModel = new CategoryModel(categoryId, categoryName);
		EcommerceResponse<CategoryModel> response = new EcommerceResponse<>(ECommerceStatus.AlreadyPresent);
		Mockito.when(categoryService.addCategory(parentCategoryId, categoryModel))
				.thenThrow(DataAlreadyPresentException.class);
		this.mockMvc
				.perform(post("/rest/category/0/sub-category").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(categoryModel)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(response)));
	}

	@Test
	public void updateCategoryTest() throws JsonProcessingException, Exception {
		CategoryModel categoryModel = new CategoryModel(categoryId, categoryName);
		EcommerceResponse<CategoryModel> response = new EcommerceResponse<>(categoryModel, ECommerceStatus.Success);
		Mockito.when(categoryService.updateCategory(categoryId, categoryModel)).thenReturn(categoryModel);
		this.mockMvc
				.perform(put("/rest/category/0/sub-category/1").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(categoryModel)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(response)));
	}

	@Test
	public void updateCategoryTestInvalidDataException() throws JsonProcessingException, Exception {
		CategoryModel categoryModel = new CategoryModel(categoryId, categoryName);
		EcommerceResponse<CategoryModel> response = new EcommerceResponse<>(ECommerceStatus.InvalidData);
		Mockito.when(categoryService.updateCategory(categoryId, categoryModel)).thenThrow(InvalidDataException.class);
		this.mockMvc
				.perform(put("/rest/category/0/sub-category/1").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(categoryModel)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(response)));
	}

	@Test
	public void updateCategoryTestDataAlreadyPresentException() throws JsonProcessingException, Exception {
		CategoryModel categoryModel = new CategoryModel(categoryId, categoryName);
		EcommerceResponse<CategoryModel> response = new EcommerceResponse<>(ECommerceStatus.AlreadyPresent);
		Mockito.when(categoryService.updateCategory(categoryId, categoryModel))
				.thenThrow(DataAlreadyPresentException.class);
		this.mockMvc
				.perform(put("/rest/category/0/sub-category/1").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(categoryModel)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(response)));
	}

	@Test
	public void updateCategoryTestCategoryException() throws JsonProcessingException, Exception {
		CategoryModel categoryModel = new CategoryModel(categoryId, categoryName);
		Mockito.when(categoryService.updateCategory(categoryId, categoryModel)).thenThrow(CategoryException.class);
		this.mockMvc.perform(put("/rest/category/0/sub-category/1").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(categoryModel))).andExpect(status().is4xxClientError());
	}

	@Test
	public void deleteCategoryTestCategoryException() throws Exception {
		Mockito.when(categoryService.deleteCategory(parentCategoryId, categoryId)).thenThrow(CategoryException.class);
		this.mockMvc.perform(delete("/rest/category/0/sub-category/1")).andExpect(status().is4xxClientError());
	}

	@Test
	public void deleteCategoryTest() throws Exception {
		CategoryModel categoryModel = new CategoryModel(categoryId, categoryName);
		Mockito.when(categoryService.deleteCategory(parentCategoryId, categoryId)).thenReturn(categoryModel);
		this.mockMvc.perform(delete("/rest/category/0/sub-category/1")).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(categoryModel)));
	}

}
