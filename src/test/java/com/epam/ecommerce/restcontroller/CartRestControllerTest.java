package com.epam.ecommerce.restcontroller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.epam.ecommerce.exception.CartException;
import com.epam.ecommerce.exception.InsufficientQuantityException;
import com.epam.ecommerce.exception.InvalidQuantityException;
import com.epam.ecommerce.exception.ProductException;
import com.epam.ecommerce.model.CartItemModel;
import com.epam.ecommerce.model.EcommerceResponse;
import com.epam.ecommerce.model.EcommerceResponse.ECommerceStatus;
import com.epam.ecommerce.model.ProductModel;
import com.epam.ecommerce.model.UserCartModel;
import com.epam.ecommerce.service.CartService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CartRestControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Mock
	private CartService cartService;

	@InjectMocks
	private CartRestController cartRestController;

	private ObjectMapper mapper = new ObjectMapper();

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(cartRestController).build();
	}

	@Test
	public void getUserCartTest() throws Exception {
		Long userId = 1L;
		UserCartModel userCartModel = new UserCartModel(1L, new ArrayList<>(), new BigDecimal("0.0"));
		Mockito.when(cartService.getUserCartModel(userId)).thenReturn(userCartModel);
		this.mockMvc.perform(get("/rest/user/1/cart"))
				.andExpect(content().json(mapper.writeValueAsString(userCartModel))).andExpect(status().isOk());
	}

	@Test
	public void addOrEditCartItemOfUserTest() throws Exception {
		Long userId = 1L;
		CartItemModel cartItemModel = new CartItemModel(1L, 1L, 1L, 1L,
				new ProductModel(1L, "Iphone X", 1L, new BigDecimal("1000.00")));
		EcommerceResponse<CartItemModel> response = new EcommerceResponse<CartItemModel>(cartItemModel,
				ECommerceStatus.Success);
		Mockito.when(cartService.addItemToCart(userId, cartItemModel)).thenReturn(
				new CartItemModel(1L, 1L, 1L, 1L, new ProductModel(1L, "Iphone X", 1L, new BigDecimal("1000.00"))));

		this.mockMvc
				.perform(post("/rest/user/1/cart").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel)))
				.andExpect(status().is2xxSuccessful()).andExpect(content().json(mapper.writeValueAsString(response)));

	}

	@Test
	public void addOrEditCartItemOfUserImproperQuantityExceptionTest() throws Exception {
		Long userId = 1L;
		CartItemModel cartItemModel = new CartItemModel(1L, 1L, 1L, 1L,
				new ProductModel(1L, "Iphone X", 1L, new BigDecimal("1000.00")));
		Mockito.when(cartService.addItemToCart(userId, cartItemModel)).thenThrow(InvalidQuantityException.class);
		EcommerceResponse<CartItemModel> response = new EcommerceResponse<CartItemModel>(
				ECommerceStatus.InvalidQuantity);
		this.mockMvc
				.perform(post("/rest/user/1/cart").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel)))
				.andExpect(content().json(mapper.writeValueAsString(response))).andExpect(status().isOk());
	}

	@Test
	public void addOrEditCartItemOfUserInsufficientQuantityExceptionTest() throws Exception {
		Long userId = 1L;
		CartItemModel cartItemModel = new CartItemModel(1L, 1L, 1L, 1L,
				new ProductModel(1L, "Iphone X", 1L, new BigDecimal("1000.00")));
		Mockito.when(cartService.addItemToCart(userId, cartItemModel)).thenThrow(InsufficientQuantityException.class);

		EcommerceResponse<CartItemModel> response = new EcommerceResponse<CartItemModel>(
				ECommerceStatus.InsufficientStock);
		this.mockMvc
				.perform(post("/rest/user/1/cart").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel)))
				.andExpect(content().json(mapper.writeValueAsString(response))).andExpect(status().isOk());
	}

	@Test
	public void addOrEditCartItemOfUserExceptionTest() throws Exception {
		Long userId = 1L;
		CartItemModel cartItemModel = new CartItemModel(1L, 1L, 1L, 1L,
				new ProductModel(1L, "Iphone X", 1L, new BigDecimal("1000.00")));
		Mockito.when(cartService.addItemToCart(userId, cartItemModel)).thenThrow(ProductException.class);
		this.mockMvc.perform(post("/rest/user/1/cart").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(cartItemModel))).andExpect(status().is4xxClientError());
	}

	@Test
	public void editCartItemQuantityTest() throws Exception {
		Long userId = 1L;
		CartItemModel cartItemModel = new CartItemModel(1L, 1L, 1L, 1L,
				new ProductModel(1L, "Iphone X", 1L, new BigDecimal("1000.00")));
		Mockito.when(cartService.editCartItemQuantity(userId, cartItemModel)).thenReturn(cartItemModel);
		EcommerceResponse<CartItemModel> response = new EcommerceResponse<CartItemModel>(cartItemModel,
				ECommerceStatus.Success);
		this.mockMvc
				.perform(put("/rest/user/1/cart/1").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel)))
				.andExpect(content().json(mapper.writeValueAsString(response))).andExpect(status().isOk());
	}

	@Test
	public void editCartItemQuantityImproperQuantityExceptionTest() throws Exception {
		Long userId = 1L;
		CartItemModel cartItemModel = new CartItemModel(1L, 1L, 1L, 1L,
				new ProductModel(1L, "Iphone X", 1L, new BigDecimal("1000.00")));
		Mockito.when(cartService.editCartItemQuantity(userId, cartItemModel)).thenThrow(InvalidQuantityException.class);
		EcommerceResponse<CartItemModel> response = new EcommerceResponse<CartItemModel>(
				ECommerceStatus.InvalidQuantity);
		this.mockMvc
				.perform(put("/rest/user/1/cart/1").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel)))
				.andExpect(content().json(mapper.writeValueAsString(response))).andExpect(status().isOk());
	}

	@Test
	public void editCartItemQuantityInsufficientQuantityExceptionTest() throws Exception {
		Long userId = 1L;
		CartItemModel cartItemModel = new CartItemModel(1L, 1L, 1L, 1L,
				new ProductModel(1L, "Iphone X", 1L, new BigDecimal("1000.00")));
		Mockito.when(cartService.editCartItemQuantity(userId, cartItemModel))
				.thenThrow(InsufficientQuantityException.class);
		EcommerceResponse<CartItemModel> response = new EcommerceResponse<CartItemModel>(
				ECommerceStatus.InsufficientStock);
		this.mockMvc
				.perform(put("/rest/user/1/cart/1").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(cartItemModel)))
				.andExpect(status().isOk()).andExpect(content().json(mapper.writeValueAsString(response)));
	}

	@Test
	public void editCartItemQuantityExceptionTest() throws Exception {
		Long userId = 1L;
		CartItemModel cartItemModel = new CartItemModel(1L, 1L, 1L, 1L,
				new ProductModel(1L, "Iphone X", 1L, new BigDecimal("1000.00")));
		Mockito.when(cartService.editCartItemQuantity(userId, cartItemModel)).thenThrow(CartException.class);
		this.mockMvc.perform(put("/rest/user/1/cart/1").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(cartItemModel))).andExpect(status().is4xxClientError());
	}

	@Test
	public void deleteCartItemFromUserTest() throws JsonProcessingException, Exception {
		Long userId = 1L;
		CartItemModel cartItemModel = new CartItemModel(1L, 1L, 1L, 1L,
				new ProductModel(1L, "Iphone X", 1L, new BigDecimal("1000.00")));
		Mockito.when(cartService.removeItemFromCart(userId, cartItemModel.getCartItemId())).thenReturn(cartItemModel);
		this.mockMvc.perform(delete("/rest/user/1/cart/1"))
				.andExpect(content().json(mapper.writeValueAsString(cartItemModel)))
				.andExpect(status().is2xxSuccessful());

	}

	@Test
	public void deleteCartItemFromUserExceptionTest() throws JsonProcessingException, Exception {
		Long userId = 1L;
		CartItemModel cartItemModel = new CartItemModel(1L, 1L, 1L, 1L,
				new ProductModel(1L, "Iphone X", 1L, new BigDecimal("1000.00")));
		Mockito.when(cartService.removeItemFromCart(userId, cartItemModel.getCartItemId()))
				.thenThrow(CartException.class);
		this.mockMvc.perform(delete("/rest/user/1/cart/1")).andExpect(status().is4xxClientError());

	}
}
