package com.epam.ecommerce.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "orders")
public class OrderItem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "order_item_id")
	private Long orderItemId;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "product_name")
	private String productName;

	@Column(name = "order_item_price")
	private BigDecimal orderItemPrice;

	@Column(name = "quantity")
	private Long quantity;

	@Column(name = "is_delivered")
	private Boolean isDelivered;

	public OrderItem() {
	}

	public OrderItem(Long orderItemId, Long userId, String productName, BigDecimal orderItemPrice, Long quantity,
			Boolean isDelivered) {
		this.orderItemId = orderItemId;
		this.userId = userId;
		this.productName = productName;
		this.orderItemPrice = orderItemPrice;
		this.quantity = quantity;
		this.isDelivered = isDelivered;
	}

	public OrderItem(Long userId, String productName, BigDecimal orderItemPrice, Long quantity, Boolean isDelivered) {
		this.userId = userId;
		this.productName = productName;
		this.orderItemPrice = orderItemPrice;
		this.quantity = quantity;
		this.isDelivered = isDelivered;
	}

	public Long getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(Long orderItemId) {
		this.orderItemId = orderItemId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getOrderItemPrice() {
		return orderItemPrice;
	}

	public void setOrderItemPrice(BigDecimal orderItemPrice) {
		this.orderItemPrice = orderItemPrice;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Boolean getIsDelivered() {
		return isDelivered;
	}

	public void setIsDelivered(Boolean isDelivered) {
		this.isDelivered = isDelivered;
	}

}
