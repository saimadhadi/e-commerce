package com.epam.ecommerce.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "category_subcategory_relation")
public class CategorySubCategoryRelation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "category_subcategory_relation_id")
	private Long categorySubCategoryRelationId;

	@Column(name = "category_id")
	private Long categoryId;

	@Column(name = "parent_category_id")
	private Long parentCategoryId;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "category_id", referencedColumnName = "category_id", insertable = false, updatable = false)
	private Category category;

	public CategorySubCategoryRelation() {

	}

	public CategorySubCategoryRelation(Long categoryId, Long parentCategoryId) {
		this.categoryId = categoryId;
		this.parentCategoryId = parentCategoryId;
	}

	public Long getCategorySubCategoryRelationId() {
		return categorySubCategoryRelationId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(Long parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setCategorySubCategoryRelationId(Long categorySubCategoryRelationId) {
		this.categorySubCategoryRelationId = categorySubCategoryRelationId;
	}

	@Override
	public String toString() {
		return "CategoryRelation [categorySubCategoryRelationId=" + categorySubCategoryRelationId + ", categoryId="
				+ categoryId + ", parentCategoryId=" + parentCategoryId + "]";
	}

}
