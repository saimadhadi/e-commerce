package com.epam.ecommerce.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "product_category_relation")
public class ProductCategoryRelation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_relation_id")
	private Long productCategoryRelationId;

	@Column(name = "product_id")
	private Long productId;

	@Column(name = "category_id")
	private Long categoryId;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "product_id", referencedColumnName = "product_id", insertable = false, updatable = false)
	private Product product;

	public ProductCategoryRelation() {
	}

	public ProductCategoryRelation(Long productId, Long categoryId) {
		this.productId = productId;
		this.categoryId = categoryId;
	}

	public Long getProductCategoryRelationId() {
		return productCategoryRelationId;
	}

	public void setProductCategoryRelationId(Long productCategoryRelationId) {
		this.productCategoryRelationId = productCategoryRelationId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "ProductRelation [productCategoryRelationId=" + productCategoryRelationId + ", productId=" + productId
				+ ", categoryId=" + categoryId + "]";
	}

}
