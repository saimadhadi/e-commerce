package com.epam.ecommerce.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "category")
public class Category {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "category_id")
	private Long categoryId;

	@Column(name = "name")
	private String name;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "parent_category_id", referencedColumnName = "category_id", insertable = false, updatable = false)
	private List<CategorySubCategoryRelation> categorySubCategoryRelationList;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "category_id", referencedColumnName = "category_id", insertable = false, updatable = false)
	private List<ProductCategoryRelation> productCategoryRelationList;

	public Category() {

	}

	public Category(String name) {
		this.name = name;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCategorySubCategoryRelationList(List<CategorySubCategoryRelation> categorySubCategoryRelationList) {
		this.categorySubCategoryRelationList = categorySubCategoryRelationList;
	}

	public List<CategorySubCategoryRelation> getCategorySubCategoryRelationList() {
		return categorySubCategoryRelationList;
	}

	public List<ProductCategoryRelation> getProductCategoryRelationList() {
		return productCategoryRelationList;
	}

	public void setProductCategoryRelationList(List<ProductCategoryRelation> productCategoryRelationList) {
		this.productCategoryRelationList = productCategoryRelationList;
	}

	@Override
	public String toString() {
		return "Category [categoryId=" + categoryId + ", name=" + name + "]";
	}

}
