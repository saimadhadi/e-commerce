package com.epam.ecommerce.model;

public class ProductCategoryRelationModel {

	private Long productCategoryRelationId;
	private Long productId;
	private Long categoryId;

	public ProductCategoryRelationModel() {
	}

	public ProductCategoryRelationModel(Long productCategoryRelationId, Long productId, Long categoryId) {
		this.productCategoryRelationId = productCategoryRelationId;
		this.productId = productId;
		this.categoryId = categoryId;
	}

	public Long getProductCategoryRelationId() {
		return productCategoryRelationId;
	}

	public void setProductCategoryRelationId(Long productCategoryRelationId) {
		this.productCategoryRelationId = productCategoryRelationId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((categoryId == null) ? 0 : categoryId.hashCode());
		result = prime * result + ((productCategoryRelationId == null) ? 0 : productCategoryRelationId.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductCategoryRelationModel other = (ProductCategoryRelationModel) obj;
		if (categoryId == null) {
			if (other.categoryId != null)
				return false;
		} else if (!categoryId.equals(other.categoryId))
			return false;
		if (productCategoryRelationId == null) {
			if (other.productCategoryRelationId != null)
				return false;
		} else if (!productCategoryRelationId.equals(other.productCategoryRelationId))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		return true;
	}

}
