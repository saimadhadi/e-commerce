package com.epam.ecommerce.model;

import java.math.BigDecimal;

import com.epam.ecommerce.entity.OrderItem;

public class OrderItemModel {

	private Long orderItemId;

	private Long userId;

	private String productName;

	private BigDecimal orderItemPrice;

	private Long quantity;

	private Boolean isDelivered;

	public OrderItemModel() {
	}

	public OrderItemModel(Long orderItemId, Long userId, String productName, BigDecimal orderItemPrice, Long quantity,
			Boolean isDelivered) {
		this.orderItemId = orderItemId;
		this.userId = userId;
		this.productName = productName;
		this.orderItemPrice = orderItemPrice;
		this.quantity = quantity;
		this.isDelivered = isDelivered;
	}

	public OrderItemModel(OrderItem orderItem) {
		this.orderItemId = orderItem.getOrderItemId();
		this.userId = orderItem.getUserId();
		this.productName = orderItem.getProductName();
		this.orderItemPrice = orderItem.getOrderItemPrice();
		this.quantity = orderItem.getQuantity();
		this.isDelivered = orderItem.getIsDelivered();
	}

	public Long getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(Long orderItemId) {
		this.orderItemId = orderItemId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getOrderItemPrice() {
		return orderItemPrice;
	}

	public void setOrderItemPrice(BigDecimal orderItemPrice) {
		this.orderItemPrice = orderItemPrice;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Boolean getIsDelivered() {
		return isDelivered;
	}

	public void setIsDelivered(Boolean isDelivered) {
		this.isDelivered = isDelivered;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((isDelivered == null) ? 0 : isDelivered.hashCode());
		result = prime * result + ((orderItemId == null) ? 0 : orderItemId.hashCode());
		result = prime * result + ((orderItemPrice == null) ? 0 : orderItemPrice.hashCode());
		result = prime * result + ((productName == null) ? 0 : productName.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		OrderItemModel other = (OrderItemModel) obj;
		if (isDelivered == null) {
			if (other.isDelivered != null) {
				return false;
			}
		} else if (!isDelivered.equals(other.isDelivered)) {
			return false;
		}
		if (orderItemId == null) {
			if (other.orderItemId != null) {
				return false;
			}
		} else if (!orderItemId.equals(other.orderItemId)) {
			return false;
		}
		if (orderItemPrice == null) {
			if (other.orderItemPrice != null) {
				return false;
			}
		} else if (!orderItemPrice.equals(other.orderItemPrice)) {
			return false;
		}
		if (productName == null) {
			if (other.productName != null) {
				return false;
			}
		} else if (!productName.equals(other.productName)) {
			return false;
		}
		if (quantity == null) {
			if (other.quantity != null) {
				return false;
			}
		} else if (!quantity.equals(other.quantity)) {
			return false;
		}
		if (userId == null) {
			if (other.userId != null) {
				return false;
			}
		} else if (!userId.equals(other.userId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "OrderItemModel [orderItemId=" + orderItemId + ", userId=" + userId + ", productName=" + productName
				+ ", orderItemPrice=" + orderItemPrice + ", quantity=" + quantity + ", isDelivered=" + isDelivered
				+ "]";
	}

}
