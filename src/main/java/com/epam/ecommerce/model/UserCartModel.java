package com.epam.ecommerce.model;

import java.math.BigDecimal;
import java.util.List;

public class UserCartModel {

	private Long userId;
	private List<UserCartItemModel> userCartItemModelList;
	private BigDecimal cartValue;

	public UserCartModel(Long userId, List<UserCartItemModel> userCartItemModelList, BigDecimal cartValue) {
		this.userId = userId;
		this.userCartItemModelList = userCartItemModelList;
		this.cartValue = cartValue;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<UserCartItemModel> getUserCartItemModelList() {
		return userCartItemModelList;
	}

	public void setUserCartItemModelList(List<UserCartItemModel> userCartItemModelList) {
		this.userCartItemModelList = userCartItemModelList;
	}
	public BigDecimal getCartValue() {
		return cartValue;
	}
	public void setCartValue(BigDecimal cartValue) {
		this.cartValue = cartValue;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cartValue == null) ? 0 : cartValue.hashCode());
		result = prime * result + ((userCartItemModelList == null) ? 0 : userCartItemModelList.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserCartModel other = (UserCartModel) obj;
		if (cartValue == null) {
			if (other.cartValue != null)
				return false;
		} else if (!cartValue.equals(other.cartValue)) {
			return false;
		}
		if (userCartItemModelList == null) {
			if (other.userCartItemModelList != null)
				return false;
		} else if (!userCartItemModelList.equals(other.userCartItemModelList)) {
			return false;
		}
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId)) {
			return false;
		}
		return true;
	}
	@Override
	public String toString() {
		return "UserCartModel [userId=" + userId + ", userCartItemModelList=" + userCartItemModelList + ", cartValue="
				+ cartValue + "]";
	}


}
