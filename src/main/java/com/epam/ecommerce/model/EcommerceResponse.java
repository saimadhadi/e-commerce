package com.epam.ecommerce.model;

public class EcommerceResponse<T> {

	private T responseObject;
	private ECommerceStatus status;

	public EcommerceResponse(T responseObject, ECommerceStatus status) {
		this.responseObject = responseObject;
		this.status = status;
	}

	public EcommerceResponse(ECommerceStatus status) {
		this.status = status;
	}

	public T getResponseObject() {
		return responseObject;
	}

	public void setResponseObject(T responseObject) {
		this.responseObject = responseObject;
	}

	public ECommerceStatus getStatus() {
		return status;
	}

	public void setStatus(ECommerceStatus status) {
		this.status = status;
	}

	public enum ECommerceStatus {
		InsufficientStock, InvalidQuantity, Success, InvalidData, AlreadyPresent;
	}
}
