package com.epam.ecommerce.model;

public class UserCartItemModel {

	private Long cartItemId;
	private ProductModel product;
	private Long quantity;

	public UserCartItemModel(Long cartItemId, ProductModel product, Long quantity) {
		this.cartItemId = cartItemId;
		this.product = product;
		this.quantity = quantity;
	}

	public Long getCartItemId() {
		return cartItemId;
	}

	public void setCartItemId(Long cartItemId) {
		this.cartItemId = cartItemId;
	}

	public ProductModel getProduct() {
		return product;
	}

	public void setProduct(ProductModel product) {
		this.product = product;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cartItemId == null) ? 0 : cartItemId.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserCartItemModel other = (UserCartItemModel) obj;
		if (cartItemId == null) {
			if (other.cartItemId != null)
				return false;
		} else if (!cartItemId.equals(other.cartItemId)) {
			return false;
		}
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product)) {
			return false;
		}
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "UserCartItemModel [cartItemId=" + cartItemId + ", product=" + product + ", quantity=" + quantity + "]";
	}

}
