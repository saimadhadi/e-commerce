package com.epam.ecommerce.model;

import java.util.List;

public class UserOrderModel {

	private Long userId;
	private List<OrderItemModel> orderItemModelList;

	public UserOrderModel(Long userId, List<OrderItemModel> orderItemModelList) {
		this.userId = userId;
		this.orderItemModelList = orderItemModelList;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<OrderItemModel> getOrderItemModelList() {
		return orderItemModelList;
	}

	public void setOrderItemModelList(List<OrderItemModel> orderItemModelList) {
		this.orderItemModelList = orderItemModelList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((orderItemModelList == null) ? 0 : orderItemModelList.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UserOrderModel other = (UserOrderModel) obj;
		if (orderItemModelList == null) {
			if (other.orderItemModelList != null) {
				return false;
			}
		} else if (!orderItemModelList.equals(other.orderItemModelList)) {
			return false;
		}
		if (userId == null) {
			if (other.userId != null) {
				return false;
			}
		} else if (!userId.equals(other.userId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "UserOrderModel [userId=" + userId + ", orderItemModelList=" + orderItemModelList + "]";
	}

}
