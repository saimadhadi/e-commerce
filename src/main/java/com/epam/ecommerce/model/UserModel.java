package com.epam.ecommerce.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.epam.ecommerce.entity.UserEntity;

public class UserModel implements UserDetails {

	private Long userId;

	private String userName;

	private String password;

	private Boolean isActive;

	private List<GrantedAuthority> authorities;

	public UserModel(UserEntity userEntity) {
		this.userId = userEntity.getUserId();
		this.userName = userEntity.getUserName();
		this.password = userEntity.getPassword();
		this.isActive = userEntity.getIsActive();
		this.authorities = Arrays.stream(userEntity.getAuthorities().split(",")).map(SimpleGrantedAuthority::new)
				.collect(Collectors.toList());
	}

	public Long getUserId() {
		return userId;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return userName;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return isActive;
	}

}
