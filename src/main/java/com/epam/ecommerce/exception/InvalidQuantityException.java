package com.epam.ecommerce.exception;

public class InvalidQuantityException extends Exception {

	public InvalidQuantityException(String message) {
		super(message);
	}

}
