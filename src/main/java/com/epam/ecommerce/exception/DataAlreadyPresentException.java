package com.epam.ecommerce.exception;

public class DataAlreadyPresentException extends Exception {

	public DataAlreadyPresentException(String message) {
		super(message);
	}
}
