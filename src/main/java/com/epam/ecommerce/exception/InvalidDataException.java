package com.epam.ecommerce.exception;

public class InvalidDataException extends Exception {
	public InvalidDataException(String message) {
		super(message);
	}
}
