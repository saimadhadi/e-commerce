package com.epam.ecommerce.exception;

public class InsufficientQuantityException extends Exception {
	public InsufficientQuantityException(String message) {
		super(message);
	}
}
