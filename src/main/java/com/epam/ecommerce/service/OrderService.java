package com.epam.ecommerce.service;

import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.ecommerce.entity.CartItem;
import com.epam.ecommerce.entity.OrderItem;
import com.epam.ecommerce.entity.Product;
import com.epam.ecommerce.exception.CartException;
import com.epam.ecommerce.exception.InsufficientQuantityException;
import com.epam.ecommerce.exception.OrderException;
import com.epam.ecommerce.model.OrderItemModel;
import com.epam.ecommerce.model.UserOrderModel;
import com.epam.ecommerce.repository.CartRepository;
import com.epam.ecommerce.repository.OrderRepository;
import com.epam.ecommerce.repository.ProductRepository;
import com.epam.ecommerce.utilities.Helper;

/**
 * Class used to manage orders component in the application
 * 
 * @author Saikumar_Madhadi
 *
 *
 */
@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private CartRepository cartRepository;

	private static final Logger log = LogManager.getLogger(OrderService.class);

	/**
	 * Method used to add order to user
	 * 
	 * @param userId
	 * @param cartItemId
	 * @return
	 * @throws CartException
	 * @throws InsufficientQuantityException
	 */
	public OrderItemModel addOrder(Long userId, Long cartItemId) throws CartException, InsufficientQuantityException {
		OrderItemModel orderItemModelAdded;
		CartItem cartItem = cartRepository.findByCartItemIdAndUserId(cartItemId, userId)
				.orElseThrow(() -> new CartException("Unable to find cart item"));
		Product product = cartItem.getProduct();
		if (product.getUnits().compareTo(cartItem.getQuantity()) < 0) {
			log.debug("Stock not available for cartItem : {}", cartItem);
			throw new InsufficientQuantityException("Stock not available");
		} else {
			BigDecimal orderItemPrice = new BigDecimal(
					cartItem.getProduct().getPrice().multiply(new BigDecimal(cartItem.getQuantity())).toString())
							.setScale(2);
			OrderItem orderItem = new OrderItem(cartItem.getUserId(), cartItem.getProduct().getName(), orderItemPrice,
					cartItem.getQuantity(), Boolean.FALSE);
			product.setUnits(product.getUnits() - cartItem.getQuantity());
			productRepository.save(product);
			orderItem = orderRepository.save(orderItem);
			cartRepository.delete(cartItem);
			orderItemModelAdded = Helper.convertOrderItemEntityToModel(orderItem);
			log.info("OrderItem {} Added ", orderItemModelAdded);
		}
		return orderItemModelAdded;
	}

	/**
	 * Method used to fetch UserOrderModel
	 * 
	 * @param userId
	 * @return
	 */
	public UserOrderModel getUserOrderModel(Long userId) {
		List<OrderItemModel> orderedItemModelList = orderRepository.findByUserId(userId).stream()
				.map(OrderItemModel::new).collect(toList());
		UserOrderModel userOrderModel = new UserOrderModel(userId, orderedItemModelList);
		log.info("User order item model list for user {} is {} ", userId, userOrderModel);
		return userOrderModel;
	}

	/**
	 * Adding all items in user cart in orders
	 * 
	 * @param userId
	 * @throws OrderException
	 */
	public void addAllOrdersFromCartForUser(Long userId) throws OrderException {
		List<CartItem> cartItemList = cartRepository.findByUserId(userId);
		if (CollectionUtils.isNotEmpty(cartItemList)) {
			for (CartItem cartItem : cartItemList) {
				BigDecimal orderItemPrice = new BigDecimal(
						cartItem.getProduct().getPrice().multiply(new BigDecimal(cartItem.getQuantity())).toString())
								.setScale(2);
				OrderItem orderItem = new OrderItem(cartItem.getUserId(), cartItem.getProduct().getName(),
						orderItemPrice, cartItem.getQuantity(), Boolean.FALSE);
				Product product = cartItem.getProduct();
				product.setUnits(product.getUnits() - cartItem.getQuantity());
				productRepository.save(product);
				orderRepository.save(orderItem);
				cartRepository.delete(cartItem);
			}
			log.info("Checked out all the items from cart {} for user : {}", cartItemList, userId);
		} else {
			log.info("Cart Item list is empty for user : {}", userId);
			throw new OrderException("No items to checkout");
		}
	}
}
