package com.epam.ecommerce.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.epam.ecommerce.entity.Category;
import com.epam.ecommerce.entity.CategorySubCategoryRelation;
import com.epam.ecommerce.exception.CategoryException;
import com.epam.ecommerce.exception.DataAlreadyPresentException;
import com.epam.ecommerce.exception.InvalidDataException;
import com.epam.ecommerce.model.CategoryModel;
import com.epam.ecommerce.repository.CategoryRepository;
import com.epam.ecommerce.repository.CategorySubCategoryRelationRepository;
import com.epam.ecommerce.utilities.Constants;
import com.epam.ecommerce.utilities.Helper;

/**
 * Class used to manage categories component in the application
 * 
 * @author Saikumar_Madhadi
 *
 */
@Service
public class CategoryService {

	@Autowired
	@Qualifier("com.epam.ecommerce.repository.CategoryRepository")
	private CategoryRepository categoryRepository;

	@Autowired
	@Qualifier("com.epam.ecommerce.repository.CategorySubCategoryRelationRepository")
	private CategorySubCategoryRelationRepository categorySubCategoryRelationRepository;

	private static final Logger log = LogManager.getLogger(CategoryService.class);

	/**
	 * Method used to fetch all the sub categories under given category
	 * 
	 * @param parentCategoryId
	 * @return
	 * @throws CategoryException
	 */
	public List<CategoryModel> getCategoriesByParentCategoryId(Long parentCategoryId) throws CategoryException {

		List<CategoryModel> categoryModelList;
		if (parentCategoryId.equals(Constants.DEFAULT_PARENT_CATEGORY_ID)) {
			List<Long> subCategoryIdList = categorySubCategoryRelationRepository.findAllSubCategoryIds();
			List<Long> categoryIdList = categoryRepository.findAllCategoryIds();
			List<Long> subCategoryIdFilteredList = (List<Long>) CollectionUtils.subtract(categoryIdList,
					subCategoryIdList);
			categoryModelList = getCategoryModelListForIdList(subCategoryIdFilteredList);
		} else {
			List<Long> subCategoryIdList = categorySubCategoryRelationRepository
					.findAllSubCategoryIdsByCategoryId(parentCategoryId);
			categoryModelList = getCategoryModelListForIdList(subCategoryIdList);
		}
		log.info("Category List under category id : {} is {}", parentCategoryId, categoryModelList);
		return categoryModelList;
	}

	private List<CategoryModel> getCategoryModelListForIdList(List<Long> subCategoryIdFilteredList)
			throws CategoryException {
		List<CategoryModel> categoryModelList = new ArrayList<>();
		for (Long subCategoryId : subCategoryIdFilteredList) {
			Optional<Category> categoryOptional = categoryRepository.findById(subCategoryId);
			if (categoryOptional.isPresent()) {
				categoryModelList.add(Helper.convertCategoryEntityToModel(categoryOptional.get()));
			} else {
				log.debug("Unable to fetch category for id: {}", subCategoryId);
				throw new CategoryException("Category Not found");
			}
		}
		return categoryModelList;
	}

	/**
	 * Method used to add category under a parent category
	 * 
	 * @param parentCategoryId
	 * @param categoryModel
	 * @return
	 * @throws DataAlreadyPresentException
	 * @throws InvalidDataException
	 */
	public CategoryModel addCategory(Long parentCategoryId, CategoryModel categoryModel)
			throws InvalidDataException, DataAlreadyPresentException {
		validateCategory(categoryModel);
		Category category = Helper.convertCategoryModelToEntity(categoryModel);
		category = categoryRepository.save(category);
		if (!parentCategoryId.equals(Constants.DEFAULT_PARENT_CATEGORY_ID)) {
			categorySubCategoryRelationRepository
					.save(new CategorySubCategoryRelation(category.getCategoryId(), parentCategoryId));
		}
		CategoryModel categoryModelAdded = Helper.convertCategoryEntityToModel(category);
		log.info("category : {} has been added", categoryModelAdded);
		return categoryModelAdded;
	}

	private void validateCategory(CategoryModel categoryModel)
			throws InvalidDataException, DataAlreadyPresentException {
		if (categoryModel.getName().length() == 0) {
			throw new InvalidDataException("Categoy name is invalid");
		}
		Optional<Category> category = categoryRepository.findByName(categoryModel.getName());
		if (category.isPresent() && categoryModel.getCategoryId() == null) {
			throw new DataAlreadyPresentException("Category with given name already present");
		}
	}

	/**
	 * Method used to delete category under given parent category
	 * 
	 * @param categoryId
	 * @return
	 * @throws CategoryException
	 */
	@Transactional
	public CategoryModel deleteCategory(Long parentCategoryId, Long categoryId) throws CategoryException {
		Category category = categoryRepository.findById(categoryId)
				.orElseThrow(() -> new CategoryException("Category not found"));
		if (!parentCategoryId.equals(Constants.DEFAULT_PARENT_CATEGORY_ID)) {
			categorySubCategoryRelationRepository.deleteByCategoryIdAndParentCategoryId(categoryId, parentCategoryId);
		}
		categoryRepository.delete(category);
		CategoryModel categoryModelDeleted = Helper.convertCategoryEntityToModel(category);
		log.info("category : {} has been deleted", categoryModelDeleted);
		return categoryModelDeleted;
	}

	/**
	 * Method used to update category
	 * 
	 * @param categoryModel
	 * @return
	 * @throws CategoryException
	 * @throws DataAlreadyPresentException
	 * @throws InvalidDataException
	 */
	public CategoryModel updateCategory(Long categoryId, CategoryModel categoryModel)
			throws CategoryException, InvalidDataException, DataAlreadyPresentException {
		Category category = categoryRepository.findById(categoryId)
				.orElseThrow(() -> new CategoryException("Category not found"));
		validateCategory(categoryModel);
		category = Helper.convertCategoryModelToEntity(categoryModel);
		category.setCategoryId(categoryId);
		category = categoryRepository.save(category);
		CategoryModel categoryModelUpdated = Helper.convertCategoryEntityToModel(category);
		log.info("category : {} has been added", categoryModelUpdated);
		return categoryModelUpdated;
	}

}
