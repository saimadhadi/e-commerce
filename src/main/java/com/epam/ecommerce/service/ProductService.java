package com.epam.ecommerce.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.ecommerce.entity.Product;
import com.epam.ecommerce.entity.ProductCategoryRelation;
import com.epam.ecommerce.exception.CategoryException;
import com.epam.ecommerce.exception.DataAlreadyPresentException;
import com.epam.ecommerce.exception.InvalidDataException;
import com.epam.ecommerce.exception.ProductException;
import com.epam.ecommerce.model.ProductModel;
import com.epam.ecommerce.repository.CategoryRepository;
import com.epam.ecommerce.repository.ProductCategoryRelationRepository;
import com.epam.ecommerce.repository.ProductRepository;
import com.epam.ecommerce.utilities.Helper;

/**
 * Class used to manage Product component in the application
 * 
 * @author Saikumar_Madhadi
 *
 */
@Service
public class ProductService {

	@Autowired
	private ProductCategoryRelationRepository productRelationRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	private static final Logger log = LogManager.getLogger(ProductService.class);

	/**
	 * Method used to fetch all the products under a given category
	 * 
	 * @param categoryId
	 * @return
	 * @throws ProductException
	 */
	public List<ProductModel> getProductsUnderCategory(Long categoryId) {
		List<ProductModel> productModelList = new ArrayList<>();
		List<ProductCategoryRelation> productCategoryRelationList = productRelationRepository
				.findByCategoryId(categoryId);
		for (ProductCategoryRelation productCategoryRelation : productCategoryRelationList) {
			productModelList.add(Helper.convertProductEntityToModel(productCategoryRelation.getProduct()));
		}
		log.info("Products under category {} are {} ", categoryId, productModelList);
		return productModelList;
	}

	/**
	 * Method used to add product
	 * 
	 * @param productModel
	 * @return
	 * @throws CategoryException
	 * @throws InvalidDataException
	 * @throws DataAlreadyPresentException
	 */
	public ProductModel addProduct(Long categoryId, ProductModel productModel)
			throws CategoryException, InvalidDataException, DataAlreadyPresentException {
		validateProduct(categoryId, productModel);
		ProductModel productModelAdded;
		Product product = Helper.convertProductModelToEntity(productModel);
		product = productRepository.save(product);
		productRelationRepository.save(new ProductCategoryRelation(product.getProductId(), categoryId));
		productModelAdded = Helper.convertProductEntityToModel(product);
		log.info("Product : {} has been added", productModelAdded);
		return productModelAdded;
	}

	/**
	 * Method used to delete product
	 * 
	 * @param productId
	 * @return
	 * @throws ProductException
	 */
	@Transactional
	public ProductModel deleteProductById(Long productId) throws ProductException {
		ProductModel productModelDeleted;
		Product product = productRepository.findById(productId)
				.orElseThrow(() -> new ProductException("Product not found"));
		productRelationRepository.deleteByProductId(productId);
		productModelDeleted = Helper.convertProductEntityToModel(product);
		log.info("Product : {} has been deleted", productModelDeleted);
		return productModelDeleted;
	}

	/**
	 * Method used to update product
	 * 
	 * @param productModel
	 * @return
	 * @throws ProductException
	 * @throws CategoryException
	 * @throws InvalidDataException
	 * @throws DataAlreadyPresentException
	 */
	public ProductModel updateProduct(Long categoryId, Long productId, ProductModel productModel)
			throws ProductException, CategoryException, InvalidDataException, DataAlreadyPresentException {
		ProductModel productModelUpdated;
		Product product = productRepository.findById(productId)
				.orElseThrow(() -> new ProductException("Product not found"));
		validateProduct(categoryId, productModel);
		product = Helper.convertProductModelToEntity(productModel);
		product.setProductId(productId);
		product = productRepository.save(product);
		productModelUpdated = Helper.convertProductEntityToModel(product);
		log.info("Product : {} has been updated", productModelUpdated);
		return productModelUpdated;
	}

	private void validateProduct(Long categoryId, ProductModel productModel)
			throws CategoryException, InvalidDataException, DataAlreadyPresentException {
		if (productModel.getUnits() < 0 || productModel.getName().length() == 0
				|| productModel.getPrice().compareTo(new BigDecimal("0")) <= 0)
			throw new InvalidDataException("Invalid Data");
		if (!categoryRepository.existsById(categoryId))
			throw new CategoryException("Category not found");
		Optional<Product> product = productRepository.findByName(productModel.getName());
		if (product.isPresent() && productModel.getProductId() == null) {
			throw new DataAlreadyPresentException("Product with given name already present");
		}
	}
}
