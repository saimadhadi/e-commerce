package com.epam.ecommerce.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.epam.ecommerce.entity.UserEntity;
import com.epam.ecommerce.model.UserModel;
import com.epam.ecommerce.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity userEntity = userRepository.findByUserName(username)
				.orElseThrow(() -> new UsernameNotFoundException("user not found"));
		return new UserModel(userEntity);
	}

	public Long getLoggedInUserId(Authentication authentication) {
		return ((UserModel) authentication.getPrincipal()).getUserId();
	}

}
