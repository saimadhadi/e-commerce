package com.epam.ecommerce.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.ecommerce.entity.CartItem;
import com.epam.ecommerce.entity.Product;
import com.epam.ecommerce.exception.CartException;
import com.epam.ecommerce.exception.InsufficientQuantityException;
import com.epam.ecommerce.exception.InvalidQuantityException;
import com.epam.ecommerce.exception.ProductException;
import com.epam.ecommerce.model.CartItemModel;
import com.epam.ecommerce.model.UserCartItemModel;
import com.epam.ecommerce.model.UserCartModel;
import com.epam.ecommerce.repository.CartRepository;
import com.epam.ecommerce.repository.ProductRepository;
import com.epam.ecommerce.utilities.Helper;

/**
 * Class used to manage the cart component in the application
 * 
 * @author Saikumar_Madhadi
 *
 */

@Service
public class CartService {

	@Autowired
	private CartRepository cartRepository;

	@Autowired
	private ProductRepository productRepository;

	private static final Logger log = LogManager.getLogger(CartService.class);

	/**
	 * Method used to add an item into the cart.
	 * 
	 * @param cartItem
	 * @return
	 * @throws CartException
	 * @throws ProductException
	 * @throws InvalidQuantityException
	 * @throws InsufficientQuantityException
	 */
	public CartItemModel addItemToCart(Long userId, CartItemModel cartItemModel)
			throws ProductException, InvalidQuantityException, InsufficientQuantityException {
		CartItemModel cartItemModelAdded;
		isValidQuantity(cartItemModel.getQuantity());
		cartItemModel.setUserId(userId);
		CartItem cartItem = Helper.convertCartItemModelToEntity(cartItemModel);
		Product product = productRepository.findById(cartItem.getProductId())
				.orElseThrow(() -> new ProductException("Product not found"));
		Optional<CartItem> cartItemExistingOptional = Optional.ofNullable(
				cartRepository.findByUserIdAndProductId(cartItemModel.getUserId(), cartItemModel.getProductId()));
		if (cartItemExistingOptional.isPresent()) {
			cartItem = editCartItem(cartItem, product, cartItemExistingOptional.get());
		} else {
			cartItem = addCartItem(cartItem, product);
		}
		cartItemModelAdded = Helper.convertCartItemEntityToModel(cartItem);
		log.info("Cart Item {} has been added to cart", cartItemModel);
		return cartItemModelAdded;
	}

	private CartItem editCartItem(CartItem cartItem, Product product, CartItem cartItemExisting)
			throws InsufficientQuantityException {
		Long updatedQuantity = cartItemExisting.getQuantity() + cartItem.getQuantity();
		isRequestedQuantityAvailable(updatedQuantity, product.getUnits());
		cartItemExisting.setQuantity(updatedQuantity);
		cartRepository.save(cartItemExisting);
		return cartItemExisting;
	}

	private CartItem addCartItem(CartItem cartItem, Product product) throws InsufficientQuantityException {
		isRequestedQuantityAvailable(cartItem.getQuantity(), product.getUnits());
		cartItem.setProduct(product);
		cartItem = cartRepository.save(cartItem);
		return cartItem;
	}

	/**
	 * Method used to remove item from the cart.
	 * 
	 * @param cartItemId
	 * @return
	 * @throws CartException
	 */
	public CartItemModel removeItemFromCart(Long userId, Long cartItemId) throws CartException {
		CartItemModel cartItemModelRemoved;
		CartItem cartItem = cartRepository.findByCartItemIdAndUserId(cartItemId, userId)
				.orElseThrow(() -> new CartException("Cart item not found"));
		cartRepository.delete(cartItem);
		cartItemModelRemoved = Helper.convertCartItemEntityToModel(cartItem);
		log.debug("CartItem {} has been removed", cartItemModelRemoved);
		return cartItemModelRemoved;
	}

	/**
	 * Method used to get UserCartModel of a particular user
	 * 
	 * @param userId
	 * @return
	 * @throws CartException
	 */
	public UserCartModel getUserCartModel(Long userId) {
		List<UserCartItemModel> userCartItemModelList = new ArrayList<>();
		BigDecimal totalPrice = new BigDecimal("0");
		List<CartItem> cartItemList = cartRepository.findByUserId(userId);
		for (CartItem cartItem : cartItemList) {
			cartItem.getProduct().setPrice(cartItem.getProduct().getPrice().setScale(2));
			totalPrice = totalPrice
					.add(cartItem.getProduct().getPrice().multiply(new BigDecimal(cartItem.getQuantity().toString())));
			userCartItemModelList.add(new UserCartItemModel(cartItem.getCartItemId(),
					Helper.convertProductEntityToModel(cartItem.getProduct()), cartItem.getQuantity()));
		}
		totalPrice = totalPrice.setScale(2);
		log.info("User cart model list of user {} is {} , and total price is {}", userId, userCartItemModelList,
				totalPrice);
		return new UserCartModel(userId, userCartItemModelList, totalPrice);
	}

	public CartItemModel editCartItemQuantity(Long userId, CartItemModel cartItemModel)
			throws CartException, InsufficientQuantityException, InvalidQuantityException {
		CartItem cartItem = cartRepository.findByCartItemIdAndUserId(cartItemModel.getCartItemId(), userId)
				.orElseThrow(() -> new CartException("Cart item not found"));
		CartItemModel cartItemModelUpdated;
		isValidQuantity(cartItemModel.getQuantity());
		isRequestedQuantityAvailable(cartItemModel.getQuantity(), cartItem.getProduct().getUnits());
		cartItem.setQuantity(cartItemModel.getQuantity());
		cartItem = cartRepository.save(cartItem);
		cartItemModelUpdated = Helper.convertCartItemEntityToModel(cartItem);
		log.info("Cart Item has been edited : {}", cartItemModelUpdated);
		return cartItemModelUpdated;
	}

	private void isRequestedQuantityAvailable(Long requestedQuantity, Long availableQuantity)
			throws InsufficientQuantityException {
		if (requestedQuantity > availableQuantity) {
			log.debug("Required quantity not available for product");
			throw new InsufficientQuantityException("Insuficient quantity");
		}
	}

	private void isValidQuantity(Long quantity) throws InvalidQuantityException {
		if (quantity <= 0) {
			log.debug("Invalid quantity passed");
			throw new InvalidQuantityException("Invalid quantity");
		}
	}
}
