package com.epam.ecommerce.restcontroller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.ecommerce.exception.CartException;
import com.epam.ecommerce.exception.InsufficientQuantityException;
import com.epam.ecommerce.exception.InvalidQuantityException;
import com.epam.ecommerce.exception.ProductException;
import com.epam.ecommerce.model.CartItemModel;
import com.epam.ecommerce.model.EcommerceResponse;
import com.epam.ecommerce.model.EcommerceResponse.ECommerceStatus;
import com.epam.ecommerce.model.UserCartModel;
import com.epam.ecommerce.service.CartService;

@RestController
@RequestMapping("/rest")
public class CartRestController {

	@Autowired
	private CartService cartService;

	private static final Logger log = LogManager.getLogger(CartRestController.class);

	@GetMapping("/user/{userId}/cart")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<UserCartModel> getUserCart(@PathVariable Long userId) {
		UserCartModel userCartModel = cartService.getUserCartModel(userId);
		log.info("Fetched user cart model for user id : {} , {}", userId, userCartModel);
		return new ResponseEntity<>(userCartModel, HttpStatus.OK);
	}

	@PostMapping("/user/{userId}/cart")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<EcommerceResponse<CartItemModel>> addOrEditCartItemOfUser(@PathVariable Long userId,
			@RequestBody CartItemModel cartItemModel) {
		CartItemModel cartItemAdded = null;
		ResponseEntity<EcommerceResponse<CartItemModel>> response;
		try {
			cartItemAdded = cartService.addItemToCart(userId, cartItemModel);
			response = new ResponseEntity<>(new EcommerceResponse<>(cartItemAdded, ECommerceStatus.Success),
					HttpStatus.CREATED);
			log.info("CartItem Added {}", cartItemAdded);
		} catch (InsufficientQuantityException e) {
			response = new ResponseEntity<>(new EcommerceResponse<>(ECommerceStatus.InsufficientStock), HttpStatus.OK);
			log.debug("Quantity requested by user id : {} is not available : {}", userId, cartItemModel, e);
		} catch (ProductException e) {
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
			log.debug("Unable to update user cart model quantity for user id : {}", userId, e);
		} catch (InvalidQuantityException e) {
			response = new ResponseEntity<>(new EcommerceResponse<CartItemModel>(ECommerceStatus.InvalidQuantity),
					HttpStatus.OK);
			log.debug("Invalid quantity has been sent by user id : {}, {}", userId, cartItemModel, e);
		}
		return response;

	}

	@PutMapping("/user/{userId}/cart/{cartItemId}")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<EcommerceResponse<CartItemModel>> editUserCartQuantity(@PathVariable Long userId,
			@RequestBody CartItemModel cartItemModel) {
		ResponseEntity<EcommerceResponse<CartItemModel>> response;
		CartItemModel cartItemModelAdded;
		try {
			cartItemModelAdded = cartService.editCartItemQuantity(userId, cartItemModel);
			response = new ResponseEntity<>(new EcommerceResponse<>(cartItemModelAdded, ECommerceStatus.Success),
					HttpStatus.OK);
			log.info("Updated user cart model quantity for user id : {} , {}", userId, cartItemModelAdded);
		} catch (CartException e) {
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
			log.debug("Unable to update user cart model quantity for user id : {}", userId, e);
		} catch (InsufficientQuantityException e) {
			response = new ResponseEntity<>(new EcommerceResponse<>(ECommerceStatus.InsufficientStock), HttpStatus.OK);
			log.debug("Quantity requested by user id : {} is not available : {}", userId, cartItemModel, e);
		} catch (InvalidQuantityException e) {
			response = new ResponseEntity<>(new EcommerceResponse<>(ECommerceStatus.InvalidQuantity), HttpStatus.OK);
			log.debug("Invalid quantity has been sent by user id : {}, {}", userId, cartItemModel, e);
		}
		return response;
	}

	@DeleteMapping("/user/{userId}/cart/{cartItemId}")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<CartItemModel> deleteItemFromUserCart(@PathVariable("userId") Long userId,
			@PathVariable("cartItemId") Long cartItemId) {
		CartItemModel cartItemModelDeleted;
		ResponseEntity<CartItemModel> response;
		try {
			cartItemModelDeleted = cartService.removeItemFromCart(userId, cartItemId);
			response = new ResponseEntity<>(cartItemModelDeleted, HttpStatus.OK);
			log.info("Deleted cart item : {} for user : {}", cartItemId, userId);
		} catch (CartException e) {
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
			log.info("unable to delete  cart item : {} for user : {}", cartItemId, userId);
		}
		return response;
	}

}
