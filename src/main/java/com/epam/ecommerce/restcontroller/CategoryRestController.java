package com.epam.ecommerce.restcontroller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.ecommerce.exception.CategoryException;
import com.epam.ecommerce.exception.DataAlreadyPresentException;
import com.epam.ecommerce.exception.InvalidDataException;
import com.epam.ecommerce.model.CategoryModel;
import com.epam.ecommerce.model.EcommerceResponse;
import com.epam.ecommerce.model.EcommerceResponse.ECommerceStatus;
import com.epam.ecommerce.service.CategoryService;

@RestController
@RequestMapping("/rest")
public class CategoryRestController {

	@Autowired
	private CategoryService categoryService;

	private static final Logger log = LogManager.getLogger(CategoryRestController.class);

	@GetMapping("/category/{categoryId}/sub-category")
	@PreAuthorize("hasRole('ROLE_ADMIN') || hasRole('ROLE_USER')")
	public ResponseEntity<List<CategoryModel>> getCategoryListUnderParentCategoryId(
			@PathVariable("categoryId") Long categoryId) {
		List<CategoryModel> categoryModelList = categoryService.getCategoriesByParentCategoryId(categoryId);
		log.info("Fetched the category list under category id : {}", categoryId);
		return new ResponseEntity<>(categoryModelList, HttpStatus.OK);
	}

	@PostMapping("/category/{categoryId}/sub-category")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<EcommerceResponse<CategoryModel>> addCategory(
			@PathVariable("categoryId") Long parentCategoryId, @RequestBody CategoryModel categoryModel) {
		ResponseEntity<EcommerceResponse<CategoryModel>> response;
		try {
			CategoryModel categoryModelAdded = categoryService.addCategory(parentCategoryId, categoryModel);
			response = new ResponseEntity<>(new EcommerceResponse<>(categoryModelAdded, ECommerceStatus.Success),
					HttpStatus.CREATED);
			log.info("Category : {} has been added", categoryModelAdded);
		} catch (InvalidDataException e) {
			log.debug("Invalid data : {} ", categoryModel, e);
			response = new ResponseEntity<>(new EcommerceResponse<>(ECommerceStatus.InvalidData), HttpStatus.OK);
		} catch (DataAlreadyPresentException e) {
			log.debug("Product with name : {} already present", categoryModel.getName(), e);
			response = new ResponseEntity<>(new EcommerceResponse<>(ECommerceStatus.AlreadyPresent), HttpStatus.OK);
		}

		return response;
	}

	@DeleteMapping("/category/{categoryId}/sub-category/{subCategoryId}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<CategoryModel> deleteCategory(@PathVariable("categoryId") Long parentCategoryId,
			@PathVariable("subCategoryId") Long categoryId) {
		ResponseEntity<CategoryModel> response;
		try {
			CategoryModel categoryModelDeleted = categoryService.deleteCategory(parentCategoryId, categoryId);
			response = new ResponseEntity<>(categoryModelDeleted, HttpStatus.OK);
			log.info("Category : {} has been deleted", categoryModelDeleted);
		} catch (CategoryException e) {
			log.debug("Category with id : {} is not found", categoryId);
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return response;
	}

	@PutMapping("/category/{categoryId}/sub-category/{subCategoryId}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<EcommerceResponse<CategoryModel>> updateCategory(
			@PathVariable("subCategoryId") Long categoryId, @RequestBody CategoryModel categoryModel) {
		ResponseEntity<EcommerceResponse<CategoryModel>> response;
		try {
			CategoryModel categoryModelUpdated = categoryService.updateCategory(categoryId, categoryModel);
			response = new ResponseEntity<>(new EcommerceResponse<>(categoryModelUpdated, ECommerceStatus.Success),
					HttpStatus.OK);
			log.info("Category : {} has been updated", categoryModelUpdated);
		} catch (CategoryException e) {
			log.debug("Category with id : {} is not found", categoryModel.getCategoryId());
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (InvalidDataException e) {
			log.debug("Invalid data : {} ", categoryModel, e);
			response = new ResponseEntity<>(new EcommerceResponse<>(ECommerceStatus.InvalidData), HttpStatus.OK);
		} catch (DataAlreadyPresentException e) {
			log.debug("Product with name : {} already present", categoryModel.getName(), e);
			response = new ResponseEntity<>(new EcommerceResponse<>(ECommerceStatus.AlreadyPresent), HttpStatus.OK);
		}
		return response;
	}
}
