package com.epam.ecommerce.restcontroller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.ecommerce.exception.CategoryException;
import com.epam.ecommerce.exception.DataAlreadyPresentException;
import com.epam.ecommerce.exception.InvalidDataException;
import com.epam.ecommerce.exception.ProductException;
import com.epam.ecommerce.model.EcommerceResponse;
import com.epam.ecommerce.model.EcommerceResponse.ECommerceStatus;
import com.epam.ecommerce.model.ProductModel;
import com.epam.ecommerce.service.ProductService;

@RestController
@RequestMapping("/rest")
public class ProductRestController {

	@Autowired
	private ProductService productService;

	private static final Logger log = LogManager.getLogger(ProductRestController.class);

	@GetMapping("/category/{categoryId}/product")
	@PreAuthorize("hasRole('ROLE_ADMIN') || hasRole('ROLE_USER')")
	public ResponseEntity<List<ProductModel>> getProductsUnderCategory(@PathVariable("categoryId") Long categoryId) {
		List<ProductModel> productList;
		productList = productService.getProductsUnderCategory(categoryId);
		log.info("Fetched products list under category : {}", categoryId);
		return new ResponseEntity<>(productList, HttpStatus.OK);
	}

	@PostMapping("/category/{categoryId}/product")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<EcommerceResponse<ProductModel>> addProductUnderCategory(
			@PathVariable("categoryId") Long categoryId, @RequestBody ProductModel productModel) {
		ResponseEntity<EcommerceResponse<ProductModel>> response;
		try {
			ProductModel productModelAdded = productService.addProduct(categoryId, productModel);
			response = new ResponseEntity<>(new EcommerceResponse<>(productModelAdded, ECommerceStatus.Success),
					HttpStatus.CREATED);
			log.info("Product : {} has been added", productModelAdded);
		} catch (InvalidDataException e) {
			log.debug("Invalid data : {} ", productModel, e);
			response = new ResponseEntity<>(new EcommerceResponse<>(ECommerceStatus.InvalidData), HttpStatus.OK);
		} catch (CategoryException e) {
			log.debug("Category with id : {} not found", categoryId, e);
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (DataAlreadyPresentException e) {
			log.debug("Product with name : {} already present", productModel.getName(), e);
			response = new ResponseEntity<>(new EcommerceResponse<>(ECommerceStatus.AlreadyPresent), HttpStatus.OK);
		}
		return response;
	}

	@PutMapping("/category/{categoryId}/product/{productId}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<EcommerceResponse<ProductModel>> updateProduct(@PathVariable("categoryId") Long categoryId,
			@PathVariable("productId") Long productId, @RequestBody ProductModel productModel) {
		ResponseEntity<EcommerceResponse<ProductModel>> response;
		try {
			ProductModel productModelUpdated = productService.updateProduct(categoryId, productId, productModel);
			response = new ResponseEntity<>(new EcommerceResponse<>(productModelUpdated, ECommerceStatus.Success),
					HttpStatus.OK);
			log.info("Product has been updated : {}", productModelUpdated);
		} catch (ProductException e) {
			log.debug("Product with id : {} not found", productModel.getProductId(), e);
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (InvalidDataException e) {
			log.debug("Invalid data : {} ", productModel, e);
			response = new ResponseEntity<>(new EcommerceResponse<>(ECommerceStatus.InvalidData), HttpStatus.OK);
		} catch (CategoryException e) {
			log.debug("Category with id : {} not found", categoryId, e);
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (DataAlreadyPresentException e) {
			log.debug("Product with name : {} already present", productModel.getName(), e);
			response = new ResponseEntity<>(new EcommerceResponse<>(ECommerceStatus.AlreadyPresent), HttpStatus.OK);
		}
		return response;
	}

	@DeleteMapping("/category/{categoryId}/product/{productId}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<ProductModel> deleteProductById(@PathVariable("productId") Long productId) {
		ResponseEntity<ProductModel> response;
		try {
			ProductModel productModelDeleted = productService.deleteProductById(productId);
			response = new ResponseEntity<>(productModelDeleted, HttpStatus.OK);
			log.info("Product has been deleted : {}", productModelDeleted);
		} catch (ProductException e) {
			log.debug("Product with id : {} not found", productId, e);
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return response;
	}

}
