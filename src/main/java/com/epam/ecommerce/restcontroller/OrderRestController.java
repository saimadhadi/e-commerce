package com.epam.ecommerce.restcontroller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.ecommerce.exception.CartException;
import com.epam.ecommerce.exception.InsufficientQuantityException;
import com.epam.ecommerce.exception.OrderException;
import com.epam.ecommerce.model.CartItemModel;
import com.epam.ecommerce.model.EcommerceResponse;
import com.epam.ecommerce.model.EcommerceResponse.ECommerceStatus;
import com.epam.ecommerce.model.OrderItemModel;
import com.epam.ecommerce.model.UserOrderModel;
import com.epam.ecommerce.service.OrderService;

@RestController
@RequestMapping("/rest")
public class OrderRestController {

	@Autowired
	private OrderService orderService;

	private static final Logger log = LogManager.getLogger(OrderRestController.class);

	@GetMapping("/user/{userId}/orders")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<UserOrderModel> getUserOrderModel(@PathVariable("userId") Long userId) {
		UserOrderModel userOrderModel = orderService.getUserOrderModel(userId);
		log.info("Fetched user order model {} for user : {}", userOrderModel, userId);
		return new ResponseEntity<>(userOrderModel, HttpStatus.OK);
	}

	@PostMapping("/user/{userId}/orders")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<EcommerceResponse<OrderItemModel>> addOrderForUser(@PathVariable Long userId,
			@RequestBody CartItemModel cartItemModel) {
		ResponseEntity<EcommerceResponse<OrderItemModel>> response;
		try {
			OrderItemModel orderItemModel = orderService.addOrder(userId, cartItemModel.getCartItemId());
			response = new ResponseEntity<>(new EcommerceResponse<>(orderItemModel, ECommerceStatus.Success),
					HttpStatus.CREATED);
			log.info("Added order item {} for user : {} ", orderItemModel, userId);
		} catch (InsufficientQuantityException e) {
			response = new ResponseEntity<>(new EcommerceResponse<>(ECommerceStatus.InsufficientStock), HttpStatus.OK);
			log.debug("Unable to add  order item,Requested quantity not available for cartitem with id : {} ",
					cartItemModel.getCartItemId(), e);
		} catch (CartException e) {
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
			log.debug("Unable to add order item, CartItem with Id : {} not found", cartItemModel.getCartItemId(), e);
		}
		return response;
	}

	@PostMapping("/user/{userId}/orders/all")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<Boolean> addAllOrdersFromCartForUser(@PathVariable Long userId) {
		ResponseEntity<Boolean> response;
		try {
			orderService.addAllOrdersFromCartForUser(userId);
			response = new ResponseEntity<>(Boolean.TRUE, HttpStatus.CREATED);
			log.info("Added all order items from cart for user : {} ", userId);
		} catch (OrderException e) {
			response = new ResponseEntity<>(Boolean.FALSE, HttpStatus.OK);
			log.info("No items in user cart : {} ", userId);
		}

		return response;
	}

}
