package com.epam.ecommerce.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.epam.ecommerce.exception.CartException;
import com.epam.ecommerce.exception.InsufficientQuantityException;
import com.epam.ecommerce.exception.OrderException;
import com.epam.ecommerce.model.OrderItemModel;
import com.epam.ecommerce.model.UserOrderModel;
import com.epam.ecommerce.service.OrderService;
import com.epam.ecommerce.service.UserDetailsServiceImpl;
import com.epam.ecommerce.utilities.Constants;

@Controller
public class OrderController {

	@Autowired
	private OrderService orderService;

	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;

	private static final Logger log = LogManager.getLogger(OrderController.class);

	@GetMapping("/orders")
	public ModelAndView getUserOrders(Authentication authentication) {
		Long userId = userDetailsServiceImpl.getLoggedInUserId(authentication);
		ModelAndView modelAndView = new ModelAndView();
		UserOrderModel userOrderModel = orderService.getUserOrderModel(userId);
		modelAndView.setViewName(Constants.ORDERS_JSP);
		modelAndView.addObject(Constants.USER_ORDER_MODEL_ATTRIBUTE, userOrderModel);
		log.info("User order model for user : {} is {}", userId, userOrderModel);
		return modelAndView;
	}

	@PostMapping("/orders")
	@ResponseBody
	public String addOrderItemForUser(Authentication authentication, @RequestBody Long cartItemId) {
		Long userId = userDetailsServiceImpl.getLoggedInUserId(authentication);
		String response;
		try {
			OrderItemModel orderItemModelAdded = orderService.addOrder(userId, cartItemId);
			response = Constants.ORDER_PLACED_MSG + orderItemModelAdded.getOrderItemId();
			log.info("OrderItem Added {}", orderItemModelAdded);
		} catch (InsufficientQuantityException e) {
			log.debug("Unable to add item due to insufficient stock", e);
			response = Constants.PRODUCT_OUT_OF_STOCK;
		} catch (CartException e) {
			log.debug("Unable to find cart item", e);
			response = Constants.ITEM_NOT_FOUND;
		}
		return response;
	}

	@PostMapping("/orders/all")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_USER')")
	public String addAllOrdersFromCartForUser(Authentication authentication) {
		Long userId = userDetailsServiceImpl.getLoggedInUserId(authentication);
		String response;
		try {
			orderService.addAllOrdersFromCartForUser(userId);
			response = Constants.CHECKED_OUT_MSG;
			log.info("Checked out all the items from cart successfully for user {}", userId);
		} catch (OrderException e) {
			log.debug("No items to check out for user : {}", userId);
			response = Constants.CART_EMPTY;
		}
		return response;
	}
}
