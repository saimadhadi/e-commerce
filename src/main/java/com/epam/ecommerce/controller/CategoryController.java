package com.epam.ecommerce.controller;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.ecommerce.exception.CategoryException;
import com.epam.ecommerce.model.CategoryModel;
import com.epam.ecommerce.model.ProductModel;
import com.epam.ecommerce.service.CategoryService;
import com.epam.ecommerce.service.ProductService;
import com.epam.ecommerce.utilities.Constants;

@Controller
public class CategoryController {

	private static final Logger log = LogManager.getLogger(CategoryController.class);

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ProductService productService;

	@GetMapping("/category")
	public ModelAndView getCategoryListOrProductList(@RequestParam("categoryId") Long categoryId) {

		ModelAndView modelAndView = new ModelAndView();
		try {
			List<CategoryModel> categoryModelList = categoryService.getCategoriesByParentCategoryId(categoryId);
			if (CollectionUtils.isNotEmpty(categoryModelList)) {
				if (categoryId.equals(Constants.DEFAULT_PARENT_CATEGORY_ID)) {
					modelAndView.addObject(Constants.CATEGORY_TYPE, Constants.CATEGORIES);
				} else {
					modelAndView.addObject(Constants.CATEGORY_TYPE, Constants.SUB_CATEGORIES);
				}
				modelAndView.addObject(Constants.CATEGORY_LIST_ATTRIBUTE, categoryModelList);
				modelAndView.setViewName(Constants.CATEGORIES_JSP);
				log.info("Fetched category List {} for category id {}", categoryModelList, categoryId);
			} else {
				List<ProductModel> productModelList = productService.getProductsUnderCategory(categoryId);
				modelAndView.addObject(Constants.PRODUCT_LIST_ATTRIBUTE, productModelList);
				modelAndView.setViewName(Constants.PRODUCTS_JSP);
				log.info("Fetched product List {} for category id {}", productModelList, categoryId);
			}
		} catch (CategoryException e) {
			modelAndView.addObject(Constants.EXCEPTION_MESSAGE, Constants.ITEM_NOT_FOUND);
			modelAndView.setViewName(Constants.ERROR_JSP);
			log.debug("Exception occured while fetching categories/products under category id : {}", categoryId);
		}
		return modelAndView;
	}

}
