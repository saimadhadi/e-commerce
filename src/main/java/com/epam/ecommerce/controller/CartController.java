package com.epam.ecommerce.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.epam.ecommerce.exception.CartException;
import com.epam.ecommerce.exception.InsufficientQuantityException;
import com.epam.ecommerce.exception.InvalidQuantityException;
import com.epam.ecommerce.exception.ProductException;
import com.epam.ecommerce.model.CartItemModel;
import com.epam.ecommerce.model.UserCartModel;
import com.epam.ecommerce.service.CartService;
import com.epam.ecommerce.service.UserDetailsServiceImpl;
import com.epam.ecommerce.utilities.Constants;

@Controller
public class CartController {

	@Autowired
	private CartService cartService;

	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;

	private static final Logger log = LogManager.getLogger(CartController.class);

	@GetMapping("/cart")
	public ModelAndView getUserCart(Authentication authentication) throws CartException {
		Long userId = userDetailsServiceImpl.getLoggedInUserId(authentication);
		ModelAndView modelAndView = new ModelAndView();
		UserCartModel userCartModel = cartService.getUserCartModel(userId);
		modelAndView.setViewName(Constants.CART_JSP);
		modelAndView.addObject(Constants.USER_CART_MODEL_ATTRIBUTE, userCartModel);
		log.info("User cart model for user : {} is {}", userId, userCartModel);
		return modelAndView;
	}

	@PostMapping("/cart")
	@PreAuthorize("hasRole('ROLE_USER')")
	public @ResponseBody String addOrEditCartItemOfUser(@RequestBody CartItemModel cartItemModel,
			Authentication authentication) {
		Long userId = userDetailsServiceImpl.getLoggedInUserId(authentication);
		String response;
		try {
			CartItemModel cartItemAdded = cartService.addItemToCart(userId, cartItemModel);
			response = Constants.ITEM_ADDED + cartItemAdded.getCartItemId();
			log.info("CartItem Added {}", cartItemAdded);
		} catch (ProductException e) {
			response = Constants.ITEM_NOT_FOUND;
		} catch (InvalidQuantityException e) {
			response = Constants.ENTER_VALID_QUANTITY;
		} catch (InsufficientQuantityException e) {
			response = Constants.PRODUCT_OUT_OF_STOCK;
		}
		return response;
	}

	@DeleteMapping("/cart")
	@ResponseBody
	public String deleteCartItemFromUser(Authentication authentication, @RequestBody Long cartItemId) {
		Long userId = userDetailsServiceImpl.getLoggedInUserId(authentication);
		String response;
		try {
			CartItemModel cartItemRemoved = cartService.removeItemFromCart(userId, cartItemId);
			response = Constants.ITEM_REMOVED;
			log.info("CartItem removed {}", cartItemRemoved);
		} catch (CartException e) {
			response = Constants.ITEM_NOT_FOUND;
			log.debug("Unable to remove cart item with id : {}", cartItemId);
		}
		return response;
	}

	@PatchMapping("/cart")
	@ResponseBody
	public String editCartItemQuantity(Authentication authentication, @RequestBody CartItemModel cartItemModel) {
		Long userId = userDetailsServiceImpl.getLoggedInUserId(authentication);
		String response;
		try {
			CartItemModel cartItemAdded = cartService.editCartItemQuantity(userId, cartItemModel);
			response = Constants.QUANTITY_UPDATED;
			log.info("CartItem quanity has been updated {}", cartItemAdded);
		} catch (CartException e) {
			response = Constants.ITEM_NOT_FOUND;
			log.debug("Unable to edit cart item quantity", e);
		} catch (InsufficientQuantityException e) {
			response = Constants.PRODUCT_OUT_OF_STOCK;
			log.debug("Required quantity not available", e);
		} catch (InvalidQuantityException e) {
			response = Constants.ENTER_VALID_QUANTITY;
		}
		return response;
	}

}
