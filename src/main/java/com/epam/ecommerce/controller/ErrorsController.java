package com.epam.ecommerce.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.epam.ecommerce.utilities.Constants;

@Controller
public class ErrorsController implements ErrorController {

	@GetMapping("/error")
	public ModelAndView handleError(HttpServletResponse response, ModelAndView modelAndView) {
		modelAndView.setViewName(Constants.ERROR_JSP);
		if (response.getStatus() == HttpStatus.NOT_FOUND.value()) {
			modelAndView.addObject(Constants.ERROR_MESSAGE, Constants.RESOURCE_NOT_FOUND);
		} else if (response.getStatus() == HttpStatus.FORBIDDEN.value()) {
			modelAndView.addObject(Constants.ERROR_MESSAGE, Constants.FORBIDDEN);
		} else if (response.getStatus() == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
			modelAndView.addObject(Constants.ERROR_MESSAGE, Constants.INTERNAL_SERVER_ERROR);
		} else {
			modelAndView.addObject(Constants.ERROR_MESSAGE, Constants.UNKNOWN_ERROR);
		}
		return modelAndView;
	}

	@Override
	public String getErrorPath() {
		return null;
	}

}
