package com.epam.ecommerce.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.epam.ecommerce.utilities.Constants;

@Controller
public class HomeController {

	@GetMapping("/home")
	public String getHomePage() {
		return Constants.INDEX_JSP;
	}
}
