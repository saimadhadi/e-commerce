package com.epam.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.ecommerce.entity.OrderItem;

/**
 * Repository for Orders
 * 
 * @author Saikumar_Madhadi
 *
 */
public interface OrderRepository extends JpaRepository<OrderItem, Long> {

	/**
	 * Fetching all the items ordered by user
	 * 
	 * @param userId
	 * @return
	 */
	public List<OrderItem> findByUserId(Long userId);
}
