package com.epam.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.epam.ecommerce.entity.CategorySubCategoryRelation;

@Repository("com.epam.ecommerce.repository.CategorySubCategoryRelationRepository")
public interface CategorySubCategoryRelationRepository extends JpaRepository<CategorySubCategoryRelation, Long> {

	/**
	 * Fetching all sub-categories mapped to the given categoryId
	 * 
	 * @param categoryId
	 * @return
	 */
	@Query("Select cscr.categoryId from CategorySubCategoryRelation cscr where cscr.parentCategoryId =:categoryId")
	List<Long> findAllSubCategoryIdsByCategoryId(@Param("categoryId") Long categoryId);

	/**
	 * Fetching all sub-category ids
	 * 
	 * @return
	 */
	@Query("Select cscr.categoryId from CategorySubCategoryRelation cscr")
	List<Long> findAllSubCategoryIds();

	/**
	 * Deleting relation of category and sub category
	 * 
	 * @param categoryId
	 * @param parentCategoryId
	 */
	void deleteByCategoryIdAndParentCategoryId(Long categoryId, Long parentCategoryId);

}
