package com.epam.ecommerce.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.ecommerce.entity.CartItem;

/**
 * Repository for Cart
 * 
 * @author Saikumar_Madhadi
 *
 */
public interface CartRepository extends JpaRepository<CartItem, Long> {

	/**
	 * Fetching all the items of a user from cart
	 * 
	 * @param userId
	 * @return
	 */
	public List<CartItem> findByUserId(Long userId);

	public CartItem findByUserIdAndProductId(Long userId, Long productId);

	public Optional<CartItem> findByCartItemIdAndUserId(Long cartItemId, Long userId);

}
