package com.epam.ecommerce.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.epam.ecommerce.entity.Category;

/**
 * Repository for Categories
 * 
 * @author Saikumar_Madhadi
 *
 */
@Repository("com.epam.ecommerce.repository.CategoryRepository")
public interface CategoryRepository extends JpaRepository<Category, Long> {

	/**
	 * Getting All the category ids
	 * 
	 * @return
	 */

	@Query("select categoryId from Category")
	public List<Long> findAllCategoryIds();

	public Optional<Category> findByName(String name);

}
