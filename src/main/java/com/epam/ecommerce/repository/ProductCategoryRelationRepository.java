package com.epam.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.ecommerce.entity.ProductCategoryRelation;

public interface ProductCategoryRelationRepository extends JpaRepository<ProductCategoryRelation, Long> {

	/**
	 * Fetch product category relations for given product
	 * 
	 * @param categoryId
	 * @return
	 */
	List<ProductCategoryRelation> findByCategoryId(Long categoryId);

	void deleteAllByProductId(Long productId);

	void deleteByProductId(Long productId);

}
