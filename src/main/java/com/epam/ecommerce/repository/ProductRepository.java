package com.epam.ecommerce.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.ecommerce.entity.Product;

/**
 * Repository for product
 * 
 * @author Saikumar_Madhadi
 *
 */
public interface ProductRepository extends JpaRepository<Product, Long> {

	Optional<Product> findByName(String name);

}
