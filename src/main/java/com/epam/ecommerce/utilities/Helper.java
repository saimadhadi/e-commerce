package com.epam.ecommerce.utilities;

import com.epam.ecommerce.entity.CartItem;
import com.epam.ecommerce.entity.Category;
import com.epam.ecommerce.entity.OrderItem;
import com.epam.ecommerce.entity.Product;
import com.epam.ecommerce.model.CartItemModel;
import com.epam.ecommerce.model.CategoryModel;
import com.epam.ecommerce.model.OrderItemModel;
import com.epam.ecommerce.model.ProductModel;

public class Helper {

	private Helper() {

	}

	public static ProductModel convertProductEntityToModel(Product product) {
		ProductModel productModel = new ProductModel();
		productModel.setProductId(product.getProductId());
		productModel.setName(product.getName());
		productModel.setPrice(product.getPrice().setScale(2));
		productModel.setUnits(product.getUnits());
		return productModel;
	}

	public static Product convertProductModelToEntity(ProductModel productModel) {
		Product product = new Product();
		product.setProductId(productModel.getProductId());
		product.setName(productModel.getName());
		product.setPrice(productModel.getPrice().setScale(2));
		product.setUnits(productModel.getUnits());
		return product;
	}

	public static CategoryModel convertCategoryEntityToModel(Category category) {
		CategoryModel categoryModel = new CategoryModel();
		categoryModel.setCategoryId(category.getCategoryId());
		categoryModel.setName(category.getName());
		return categoryModel;
	}

	public static Category convertCategoryModelToEntity(CategoryModel categoryModel) {
		Category category = new Category();
		category.setCategoryId(categoryModel.getCategoryId());
		category.setName(categoryModel.getName());
		return category;
	}

	public static CartItemModel convertCartItemEntityToModel(CartItem cartItem) {
		CartItemModel cartItemModel = new CartItemModel();
		cartItemModel.setCartItemId(cartItem.getCartItemId());
		cartItemModel.setProductId(cartItem.getProductId());
		cartItemModel.setQuantity(cartItem.getQuantity());
		cartItemModel.setUserId(cartItem.getUserId());
		cartItemModel.setProductModel(convertProductEntityToModel(cartItem.getProduct()));
		return cartItemModel;
	}

	public static CartItem convertCartItemModelToEntity(CartItemModel cartItemModel) {
		CartItem cartItem = new CartItem();
		cartItem.setProductId(cartItemModel.getProductId());
		cartItem.setQuantity(cartItemModel.getQuantity());
		cartItem.setUserId(cartItemModel.getUserId());
		return cartItem;
	}

	public static OrderItemModel convertOrderItemEntityToModel(OrderItem orderItem) {
		OrderItemModel orderItemModel = new OrderItemModel();
		orderItemModel.setOrderItemId(orderItem.getOrderItemId());
		orderItemModel.setUserId(orderItem.getUserId());
		orderItemModel.setIsDelivered(orderItem.getIsDelivered());
		orderItemModel.setQuantity(orderItem.getQuantity());
		orderItemModel.setProductName(orderItem.getProductName());
		orderItemModel.setOrderItemPrice(orderItem.getOrderItemPrice());
		return orderItemModel;
	}

}
