package com.epam.ecommerce.utilities;

public class Constants {

	private Constants() {
	}

	public static final Long DEFAULT_PARENT_CATEGORY_ID = 0L;
	public static final String CHOICE_YES = "Y";

	public static final String CART_ITEM_ID = "cart_item_id";
	public static final String USER_ID = "user_id";
	public static final String PRODUCT_ID = "product_id";
	public static final String QUANTITY = "quantity";
	public static final String CATEGORY_ID = "category_id";
	public static final String NAME = "name";
	public static final String PRICE = "price";
	public static final String UNITS = "units";
	public static final String ORDER_ITEM_ID = "order_item_id";
	public static final String IS_DELIVERED = "is_delivered";
	public static final String CATEGORY_TYPE = "categoryType";
	public static final String CATEGORIES = "Categories";
	public static final String SUB_CATEGORIES = "Sub Categories";

	public static final String USER_CART_MODEL_ATTRIBUTE = "userCartModel";
	public static final String USER_ORDER_MODEL_ATTRIBUTE = "userOrderModel";
	public static final String CART_ITEM_ID_ATTRIBUTE = "cartItemId";
	public static final String CATEGORY_LIST_ATTRIBUTE = "categoryList";
	public static final String PRODUCT_LIST_ATTRIBUTE = "productList";

	public static final String USER_ID_REQUEST_PARAM = "user-id";
	public static final String CATEGORY_ID_REQUEST_PARAM = "category-id";

	public static final String CART_JSP = "jsp/cart.jsp";
	public static final String ORDERS_JSP = "jsp/orders.jsp";
	public static final String CATEGORIES_JSP = "jsp/categories.jsp";
	public static final String PRODUCTS_JSP = "jsp/products.jsp";
	public static final String ERROR_JSP = "jsp/error.jsp";
	public static final String INDEX_JSP = "jsp/index.jsp";

	public static final String ERROR_MESSAGE = "errorMessage";
	public static final String EXCEPTION_MESSAGE = "exceptionMessage";
	public static final String RESOURCE_NOT_FOUND = "Resource Not Found";
	public static final String FORBIDDEN = "Forbidden";
	public static final String INTERNAL_SERVER_ERROR = "Internal server error";
	public static final String UNKNOWN_ERROR = "Unknown Error";
	public static final String PRODUCT_OUT_OF_STOCK = "Product Out of stock";
	public static final String ENTER_VALID_QUANTITY = "Enter valid Quantity";
	public static final String QUANTITY_UPDATED = "Quantity has been Updated";
	public static final String ITEM_REMOVED = "Item removed";
	public static final String ITEM_ADDED = "Your Item has been added to cart with id : ";
	public static final String ORDER_PLACED_MSG = "Your order has been placed with id : ";
	public static final String CHECKED_OUT_MSG = "Checked out all the items from cart successfully";
	public static final String UNABLE_TO_CHECK_OUT = "Unable to checkout";
	public static final String ITEM_NOT_FOUND = "Item not found";
	public static final String CART_EMPTY = "Cart is empty";

	public static final String ZERO_STRING = "0";
	public static final String ONE_STRING = "1";
	public static final String TWO_STRING = "2";

}
