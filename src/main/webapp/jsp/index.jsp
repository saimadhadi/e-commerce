<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Home</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<body>
<div class="jumbotron text-center" style="margin-bottom:0">
  <h1>Welcome to E-Commerce Application</h1>
</div>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link active" href="/home">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/category?categoryId=0">Shop</a>
      </li>
      <sec:authorize access="hasRole('USER')">
		<li class="nav-item">
        	<a class="nav-link" href="/cart">Cart</a>
      	</li>
	      <li class="nav-item">
	        <a class="nav-link" href="/orders">Orders</a>
	      </li>
		</sec:authorize>

    </ul>
  </div>
</nav>

<div class="card text-center">
  <div class="card-header">
  </div>
  <div class="card-body">
    <h5 class="card-title">Free Delivery</h5>
    <p class="card-text"></p>
  </div>
  <div class="card-footer text-muted">
  </div>
</div>

<div class="card text-center">
  <div class="card-header">
  </div>
  <div class="card-body">
    <h5 class="card-title">24/7 Support</h5>
    <p class="card-text"></p>
  </div>
  <div class="card-footer text-muted">
  </div>
</div>

<div class="card text-center">
  <div class="card-header">
  </div>
  <div class="card-body">
    <h5 class="card-title">Secure Payment</h5>
    <p class="card-text"></p>
  </div>
  <div class="card-footer text-muted">
  </div>
</div>
</body>
</html>
