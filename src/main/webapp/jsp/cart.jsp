<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Cart</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<body>
<div class="jumbotron text-center" style="margin-bottom:0">
  <h1>Welcome to E-Commerce Application</h1>
</div>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="/home">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/category?categoryId=0">Shop</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="/cart">Cart<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/orders">Orders</a>
      </li>
    </ul>
  </div>
</nav>

<br>
<c:choose>
    <c:when test="${empty userCartModel.userCartItemModelList}">
       <h2 class="text-center">Your cart is empty...!!!</h2>
    </c:when>    
    <c:otherwise>
    	<div class="card-columns">
	        <c:forEach var="userCartItemModel"  items="${userCartModel.userCartItemModelList}">
	        <div class="card bg-light mb-3" style="max-width: 18rem;">
  				<div class="card-body">
    				<h5 class="card-title">${userCartItemModel.product.name}</h5>
    				<p class="card-text">${userCartItemModel.product.price} $</p>
    				<div class="input-group input-group-sm mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text" id="inputGroup-sizing-sm">Quantity</span>
					  </div>
						<input class="card-text form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" type="number" autocomplete="off"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
    type = "number"onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" id="quantity${userCartItemModel.product.productId}" name="quantity${product.productId}" required min="1" max = "999" maxlength="3" value="${userCartItemModel.quantity}">
					</div>
					<button class="card-text" onclick="editCartItem(${userCartItemModel.cartItemId},${userCartItemModel.product.productId})" id="product${userCartItemModel.product.productId}">Update</button>
					<button class="card-text" onclick="placeOrder(${userCartItemModel.cartItemId},${userCartModel.userId},${userCartItemModel.product.productId},${userCartItemModel.quantity})" id="product${userCartItemModel.product.productId}">Place order</button>
					<button class="card-text" onclick="removeItemFromCart(${userCartItemModel.cartItemId})" id="product${userCartItemModel.product.productId}">Remove</button>
  				</div>
			</div>			
			</c:forEach>
		</div>
		<p>Total Cart Price = ${userCartModel.cartValue}$ </p>
		<button onclick="checkOut(${userCartModel.userId})" type="button" class="btn btn-primary btn-lg btn-block">Check Out</button>
 	 		<button style="visibility:hidden;" id="rc" type="button"  data-toggle="modal" data-target="#exampleModalCenter">
			</button>
 	 		<div class="modal fade" data-backdrop="static" data-keyboard="false" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			  <div class="modal-dialog modal-dialog-centered" role="document">
			    <div class="modal-content">
			      <div class="modal-body" id="responseBody"></div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" onclick="location.reload()" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			  </div>
			</div>
    </c:otherwise>
</c:choose>


<script type="text/javascript">
	function placeOrder(ciid) {
		var xhr = new XMLHttpRequest();
		xhr.addEventListener("load", function(){
			document.getElementById("responseBody").innerHTML =  this.responseText;	
			document.getElementById("rc").click();
		})
		xhr.open("POST", "/orders", true);
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send(JSON.stringify(ciid));
		
	}
	
	function removeItemFromCart(ciid) {
		var xhr = new XMLHttpRequest();
		xhr.addEventListener("load", function(){
			location.reload();
		});
		xhr.open("DELETE", "/cart", true);
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send(JSON.stringify(ciid));
		
	}
	
	function editCartItem(ciid,pid) {
		var qty = +document.getElementById("quantity"+pid).value;
		var cartItem = {cartItemId:ciid,quantity:qty};
		var xhr = new XMLHttpRequest();
		xhr.addEventListener("load", function() {
			document.getElementById("responseBody").innerHTML =  this.responseText;	
			document.getElementById("rc").click();
		})
		xhr.open("PATCH", "/cart", true);
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send(JSON.stringify(cartItem));
	}
	
	function checkOut(uid) {
		var xhr = new XMLHttpRequest();
		xhr.addEventListener("load", function(){
			document.getElementById("responseBody").innerHTML =  this.responseText;	
			document.getElementById("rc").click();				
		})
		xhr.open("POST", "/orders/all", true);
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send(JSON.stringify(uid));	
	}
	
</script>
</body>
</html>