<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Products</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<body>
<div class="jumbotron text-center" style="margin-bottom:0">
  <h1>Welcome to E-Commerce Application</h1>
</div>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="/home">Home</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="/category?categoryId=0">Shop</a>
      </li>
      <sec:authorize access="hasRole('USER')">
      <li class="nav-item">
        <a class="nav-link" href="/cart">Cart</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/orders">Orders</a>
      </li>
      </sec:authorize>
    </ul>
  </div>
</nav>
<c:choose>
	<c:when test="${empty productList}">
		<sec:authorize access="hasRole('USER')">
       <h2 class="text-center">No Products Available under this category...!!!</h2>
       </sec:authorize>
       <sec:authorize access="hasRole('ADMIN')">
       <br>
       <br>
		<div class="card-columns">
			<div class="card bg-light mb-3" style="max-width: 18rem;">
		  		<div class="card-body">

					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addCategory">
				  	Add Category
					</button>
					</div>
					<div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel">Category</h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="location.reload()">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
							<div class="input-group mb-3">
							  <div class="input-group-prepend">
							    <span class="input-group-text" id="inputGroup-sizing-default">Name</span>
							  </div>
							  <input type="text" class="form-control" id="cname" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" minlength="4" required>
							</div>
							<p id="responseBody"></p>	        
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
					        <button type="button" onclick="addCategory()" class="btn btn-primary">Save</button>
					      </div>
					    </div>
					  </div>
					</div>

		  		</div>
		  		<div class="card bg-light mb-3" style="max-width: 18rem;">
		  			<div class="card-body">
	    				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addProduct">
 						Add Product
						</button>	
						<div class="modal fade" id="addProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel">Product</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="location.reload()">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body">
								<div class="input-group mb-3">
								  <div class="input-group-prepend">
								    <span class="input-group-text" id="inputGroup-sizing-default">Name</span>
								  </div>
								  <input type="text" class="form-control" id="pname" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" required>
								</div>
								<div class="input-group mb-3">
								  <div class="input-group-prepend">
								    <span class="input-group-text" id="inputGroup-sizing-default">Quantity</span>
								  </div>
								  <input type="number" class="form-control" id="pqty" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required min="1" max = "9999999" maxlength="6" >
								</div>
								<div class="input-group mb-3">
								  <div class="input-group-prepend">
								    <span class="input-group-text">$</span>
								  </div>
								  <input type="number" class="form-control" id="pprice" aria-label="Amount (to the nearest dollar)" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required min="1" max = "9999999" maxlength="7">
								</div>
								 <p id="responseBody2"></p>	        
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
						        <button type="button" onclick="addProduct()" class="btn btn-primary">Save</button>
						      </div>
						    </div>
						  </div>
						</div>			
		  			</div>
				</div>
			</div>
       </sec:authorize>
	 </c:when>
 	<c:otherwise>
		<h2 class="text-center">Products</h2>
		<div class="card-columns">
		<c:forEach var="product"  items="${productList}">
		<div class="card bg-light mb-3" style="max-width: 18rem;">
		 		<div class="card-body">
		   		<h5 class="card-title">${product.name}</h5>
		   		<p class="card-text">${product.price} $</p>
		  			<sec:authorize access="hasRole('USER')">
				<div class="input-group input-group-sm mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text" id="inputGroup-sizing-sm">Quantity</span>
					</div>
					<input class="card-text form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" type="number"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
		   type = "number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" id="quantity${product.productId}" name="quantity${product.productId}" required min="1" max = "999" maxlength="3" value="1">
				</div>
		   		<button class="btn btn-primary" onclick="addItemToCart(${product.productId})" id="product${product.productId}">add to cart</button>
				</sec:authorize>
				<sec:authorize access="hasRole('ADMIN')">
				<p class="card-text">Units in stock : ${product.units}</p>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editProduct${product.productId}">
		 			Edit
				</button>
				<button class="btn btn-primary" onclick="deleteProduct(${product.productId})">Delete</button>
				<div class="modal fade" id="editProduct${product.productId}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Product</h5>
				        <button type="button" class="close" onclick="location.reload()" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
						<div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="inputGroup-sizing-default">Name</span>
						  </div>
						  <input type="text" class="form-control" id="pname${product.productId}" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="${product.name}">
						</div>
						<div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="inputGroup-sizing-default">Quantity</span>
						  </div>
						  <input type="number" class="form-control" id="pqty${product.productId}" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required min="1" max = "999999" maxlength="6" value="${product.units}">
						</div>
						<div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">$</span>
						  </div>
						  <input type="number" class="form-control" id="pprice${product.productId}" aria-label="Amount (to the nearest dollar)" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required min="1" max = "999999" maxlength="7" value="${product.price}">
						</div>
						 <p id="responseBody3${product.productId}"></p>	        
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" onclick="location.reload()" data-dismiss="modal">Close</button>
				        <button type="button" onclick="editProduct(${product.productId})" class="btn btn-primary">Save</button>
				      </div>
				    </div>
				  </div>
				</div>
		
				</sec:authorize>
		 		</div>
		</div>
		</c:forEach>
		</div>
		<button style="visibility:hidden;" id="rc" type="button"  data-toggle="modal" data-target="#exampleModalCenter">
		</button>
		<div class="modal fade" data-backdrop="static" data-keyboard="false" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
		    	<div class="modal-content">
		      		<div class="modal-body" id="responseBody"></div>
			    	<div class="modal-footer">
			        	<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
			      	</div>
		    	</div>
		  	</div>
		</div>

<br>
<sec:authorize access="hasRole('ADMIN')">
	<div class="text-center"> 
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  	Add
	</button>
	</div>
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Product</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="location.reload()">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
			<div class="input-group mb-3">
			  <div class="input-group-prepend">
			    <span class="input-group-text" id="inputGroup-sizing-default">Name</span>
			  </div>
			  <input type="text" class="form-control" id="pname" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" required>
			</div>
			<div class="input-group mb-3">
			  <div class="input-group-prepend">
			    <span class="input-group-text" id="inputGroup-sizing-default">Quantity</span>
			  </div>
			  <input type="number" class="form-control" id="pqty" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required min="1" max = "9999999" maxlength="6" >
			</div>
			<div class="input-group mb-3">
			  <div class="input-group-prepend">
			    <span class="input-group-text">$</span>
			  </div>
			  <input type="number" class="form-control" id="pprice" aria-label="Amount (to the nearest dollar)" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required min="1" max = "9999999" maxlength="7">
			</div>
			 <p id="responseBody2"></p>	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
	        <button type="button" onclick="addProduct()" class="btn btn-primary">Save</button>
	      </div>
	    </div>
	  </div>
	</div>
</sec:authorize>
	</c:otherwise>
</c:choose>
</body>

<script type="text/javascript">
function addItemToCart(pid) {
	var qty = +document.getElementById("quantity"+pid).value;
	var cartItem = {productId:pid,quantity:qty};
	var xhr = new XMLHttpRequest();
	xhr.addEventListener("load", function() {
		document.getElementById("responseBody").innerHTML =  this.responseText;	
		document.getElementById("rc").click();
	})
	xhr.open("POST", "/cart", true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify(cartItem));
}

function deleteProduct(pid) {
	var params = (new URL(document.location)).searchParams;
	var categoryId = params.get("categoryId");
	var xhr = new XMLHttpRequest();
	xhr.addEventListener("load", function() {
		//var responseBody = JSON.parse(this.response);
		document.getElementById("responseBody").innerHTML =  "Product has been deleted";	
		document.getElementById("rc").click();
	})
	xhr.open("DELETE", "/rest/category/"+categoryId+"/product/"+pid, true);
	xhr.send();
}

function addProduct() {
	var params = (new URL(document.location)).searchParams;
	var categoryId = params.get("categoryId");
	var pname = document.getElementById("pname").value;
	var pqty = +document.getElementById("pqty").value;
	var pprice = +document.getElementById("pprice").value;
	if(pname.length==0) {
		document.getElementById("responseBody2").innerHTML =  "Enter Valid Name";
		return ;
	}
	if(pqty<=0) {
		document.getElementById("responseBody2").innerHTML =  "Enter Valid Quantity";
		return ;
	}
	if(pprice<=0) {
		document.getElementById("responseBody2").innerHTML =  "Enter Valid Price";
		return ;
	}
	var product = {name:pname,units:pqty,price:pprice};
	var xhr = new XMLHttpRequest();
	xhr.addEventListener("load", function() {
		var responseBody = JSON.parse(this.response);
		document.getElementById("responseBody2").innerHTML =  responseBody.status;	
	})
	xhr.open("POST", "/rest/category/"+categoryId+"/product/", true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify(product));
}

function editProduct(pid) {
	var params = (new URL(document.location)).searchParams;
	var categoryId = params.get("categoryId");
	var pname = document.getElementById("pname"+pid).value;
	var pqty = +document.getElementById("pqty"+pid).value;
	var pprice = +document.getElementById("pprice"+pid).value;
	if(pname.length==0) {
		document.getElementById("responseBody3"+pid).innerHTML =  "Enter Valid Name";
		return ;
	}
	if(pqty<=0) {
		document.getElementById("responseBody3"+pid).innerHTML =  "Enter Valid Quantity";
		return ;
	}
	if(pprice<=0) {
		document.getElementById("responseBody3"+pid).innerHTML =  "Enter Valid Price";
		return ;
	}
	var product = {productId:pid,name:pname,units:pqty,price:pprice};
	var xhr = new XMLHttpRequest();
	xhr.addEventListener("load", function() {
		var responseBody = JSON.parse(this.response);
		document.getElementById("responseBody3"+pid).innerHTML =  responseBody.status;	
	})
	xhr.open("PUT", "/rest/category/"+categoryId+"/product/"+pid, true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify(product));
}

function addCategory() {
	let params = (new URL(document.location)).searchParams;
	let pcid = params.get("categoryId");
	var cName = document.getElementById("cname").value;
	if(cName.length==0) {
		document.getElementById("responseBody").innerHTML =  "Enter Valid Name";
		return ;
	}
	var category = {name:cName};
	var xhr = new XMLHttpRequest();
	xhr.addEventListener("load", function() {
		if(xhr.status==201){
			document.getElementById("responseBody").innerHTML =  "Success";
		} else {
			document.getElementById("responseBody").innerHTML =  "Something went wrong.";
		}
	})
	xhr.open("POST", "/rest/category/"+pcid+"/sub-category", true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify(category));
}
</script>
</html>