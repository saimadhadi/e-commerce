<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Orders</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<body>
<div class="jumbotron text-center" style="margin-bottom:0">
  <h1>Welcome to E-Commerce Application</h1>
</div>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="/home">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/category?categoryId=0">Shop</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/cart">Cart</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="/orders">Orders</a>
      </li>
    </ul>
  </div>
</nav>
<br>
<c:choose>
    <c:when test="${empty userOrderModel.orderItemModelList}">
       <h2 class="text-center">You have not ordered anything...!!!</h2>
    </c:when>    
    <c:otherwise>
    	<div class="card-columns">
	        <c:forEach var="orderItemModel"  items="${userOrderModel.orderItemModelList}">
	        <div class="card bg-light mb-3" style="max-width: 18rem;">
  				<div class="card-body">
    				<h5 class="card-title">${orderItemModel.productName}</h5>
    				<p class="card-text"> ${orderItemModel.orderItemPrice} $</p>
    				<p class="card-text">Quantity : ${orderItemModel.quantity}</p>
  				</div>
			</div>
			</c:forEach>
			</div>
    </c:otherwise>
</c:choose>
</body>
</html>