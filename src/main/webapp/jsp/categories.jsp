<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Categories</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<body>
<div class="jumbotron text-center" style="margin-bottom:0">
  <h1>Welcome to E-Commerce Application</h1>
</div>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="/home">Home</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="/category?categoryId=0">Shop</a>
      </li>
      <sec:authorize access="hasRole('USER')">
      <li class="nav-item">
        <a class="nav-link" href="/cart">Cart</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/orders">Orders</a>
      </li>
      </sec:authorize>
    </ul>
  </div>
</nav>

<h2 class="text-center">${categoryType}</h2>
<br>
<div class="card-columns">
	<c:forEach var="category"  items="${categoryList}">
		<div class="card bg-light mb-3" style="max-width: 18rem;">
	  		<div class="card-body">
	    		<a class="card-title" href = "/category?categoryId=${category.categoryId}">
				<c:out value="${category.name}"></c:out>
				</a>
				<sec:authorize access="hasRole('ADMIN')">
				<br><br>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editCategory${category.categoryId}">
		  			Edit
					</button>
					<button class="btn btn-primary" onclick="deleteCategory(${category.categoryId})">Delete</button>
				<div class="modal fade" id="editCategory${category.categoryId}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Category</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="location.reload()">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
						<div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="inputGroup-sizing-default">Name</span>
						  </div>
						  <input type="text" class="form-control" id="cname${category.categoryId}" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" minlength="4" required value="${category.name}">
						</div>
						<p id="responseBody${category.categoryId}"></p>	        
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
				        <button type="button" onclick="editCategory(${category.categoryId})" class="btn btn-primary">Save</button>
				      </div>
				    </div>
				  </div>
				</div>
				</sec:authorize>
	  		</div>
		</div>
	</c:forEach>
</div>
<sec:authorize access="hasRole('ADMIN')">
	<div class="text-center">  
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  	Add
	</button>
	</div>
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Category</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="location.reload()">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
			<div class="input-group mb-3">
			  <div class="input-group-prepend">
			    <span class="input-group-text" id="inputGroup-sizing-default">Name</span>
			  </div>
			  <input type="text" class="form-control" id="cname" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" minlength="4" required>
			</div>
			<p id="responseBody"></p>	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
	        <button type="button" onclick="addCategory()" class="btn btn-primary">Save</button>
	      </div>
	    </div>
	  </div>
	</div>
	
	<button style="visibility:hidden;" id="rc" type="button"  data-toggle="modal" data-target="#deletemsg">
</button>
<div class="modal fade" data-backdrop="static" data-keyboard="false" id="deletemsg" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
    	<div class="modal-content">
      		<div class="modal-body" id="responseBody2">
      		</div>
	    	<div class="modal-footer">
	        	<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
	      	</div>
    	</div>
  	</div>
</div>
</sec:authorize>
</body>
<script type="text/javascript">
function addCategory() {
	let params = (new URL(document.location)).searchParams;
	let pcid = params.get("categoryId");
	var cName = document.getElementById("cname").value;
	if(cName.length==0) {
		document.getElementById("responseBody").innerHTML =  "Enter Valid Name";
		return ;
	}
	var category = {name:cName};
	var xhr = new XMLHttpRequest();
	xhr.addEventListener("load", function() {
		if(xhr.status==201){
			document.getElementById("responseBody").innerHTML =  "Success";
		} else {
			document.getElementById("responseBody").innerHTML =  "Something went wrong.";
		}
	})
	xhr.open("POST", "/rest/category/"+pcid+"/sub-category", true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify(category));
}

function deleteCategory(cid) {
	let params = (new URL(document.location)).searchParams;
	let pcid = params.get("categoryId");
	var xhr = new XMLHttpRequest();
	xhr.addEventListener("load", function() {
		location.reload();
		
	})
	xhr.open("DELETE", "/rest/category/"+pcid+"/sub-category/"+cid, true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send();
}

function editCategory(cid) {
	let params = (new URL(document.location)).searchParams;
	let pcid = params.get("categoryId");
	var cName = document.getElementById("cname"+cid).value;
	if(cName.length==0) {
		document.getElementById("responseBody"+cid).innerHTML =  "Enter Valid Name";
		return ;
	}
	var category = {name:cName};
	var xhr = new XMLHttpRequest();
	xhr.addEventListener("load", function() {
		if(xhr.status==200){
			document.getElementById("responseBody"+cid).innerHTML =  "Success";
		} else {
			document.getElementById("responseBody"+cid).innerHTML =  "Something went wrong.";
		}
	})
	xhr.open("PUT", "/rest/category/"+pcid+"/sub-category/"+cid, true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify(category));
}
</script>

</html>